# Fin Hub
Fin Hub is a web application that implements double-entry bookkeeping for personal use. It helps you manage all your accounts 
in one place, and builds quarterly financial statements.

[![pipeline status](https://gitlab.com/fin-truth/fin-hub/badges/main/pipeline.svg)](https://gitlab.com/fin-truth/fin-hub/-/commits/main)
[![API coverage report](https://gitlab.com/fin-truth/fin-hub/badges/main/coverage.svg)](https://gitlab.com/fin-truth/fin-hub/-/commits/main)
[![Latest Release](https://img.shields.io/badge/dynamic/json?color=blue&label=release&query=%24%5B0%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F13102992%2Freleases)](https://gitlab.com/fin-truth/fin-hub/-/releases)

## Installation
1. Install [Docker](https://docs.docker.com/engine/install/)
2. Authenticate with the Gitlab registry by running `docker login registry.gitlab.com` - note you may need to create a Gitlab PAT for this authentication step. See [here](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry)
3. Clone the repo
4. `docker compose up` to launch a local production version of the Flask app on port 80 with the API server on 8080

## Getting started
The app relies on having a seeded SQLite database. On first clone, you can create the basic DB with `uv run fin-hub seed` from the backend project.
This will create a basic, empty DB in the `backend/instance` folder, which is not git committed, for the app to have a starting point.

Thereafter, make sure your `app.db` is in the instance folder and the app will initialize against the data in that folder.

`backend/fin-utils` has a few other helper scripts for exporting and importing the SQLite data, mostly to assist with schema migrations.

## Development
There is a dev container defined for each project in this monorepo. Make sure to change the git login information in the 
`.devcontainer/devcontainer.json`, and to have an SSH key registered and available in ~/.ssh. The devcontainer 
will run the actual project's Dockerfile via a dev docker compose. Make sure to add new services to both the prod 
docker compose and the dev docker compose. A debug config for the flask server is setup in `.vscode/launch.json` as well.

You can also develop locally by first moving to the project directory and installing dependencies with `uv sync --frozen` and then `uv run flask run --host 0.0.0.0`.

In either case, the development API server or frontend server will be available on port 5000. It is recommended to instead open two VSCode instances with the devcontainers so that all inter-container networking is set up for you, though.

## Deployment
This app is a webapp and can be deployed by any of the standard ways, but to deploy it locally for LAN access, the `docker-compose.yml` 
provides the proper configuration to run a multiworker gunicorn server for the application. It will host the frontend on port 80. The containers are hosted in the Gitlab project's registry.

To ease local deployment further on Debian systems, consider:
- Creating an entry in `/etc/hosts` like `127.0.2.1        fin-hub.local` for local name resolution.
- Creating a service that will start the docker compose file on boot. See an example service file below:
```
> cat /etc/systemd/system/fin-hub.service
[Unit]
Description=Fin Hub Production Deployment
After=docker.service
Requires=docker.service

[Service]
Type=oneshot
RemainAfterExit=yes
WorkingDirectory=/home/joe/repos/personal/fin-hub
ExecStart=/usr/bin/docker compose up --detach
ExecStop=/usr/bin/docker compose down
TimeoutSec=30

[Install]
WantedBy=multi-user.target
```
- You can enable it with `sudo systemctl enable fin-hub.service`. It will now start on boot.
- You can view logs with `docker compose logs -f`

### Deploying a new Production Version
Merge a new version of the app to main. This will trigger a new build and push to the registry. Then, update your tag reference in the docker compose file to the new version (or use latest). 

1. `sudo systemctl stop fin-hub.service`
2. `docker compose pull`
3. `sudo systemctl start fin-hub.service`
