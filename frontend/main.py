from fin_frontend import create_app

app = create_app(environment="PRODUCTION")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
