# Frontend
This is the frontend of the Fin Hub application. It is a Flask app that uses Jinja2 templates and Bootstrap for styling.

## Getting Started
It is strongly recommended to just use the devcontainer. The `uv` environment will already be set up within this container. If you are developing locally, 
1. Install [uv](https://docs.astral.sh/uv/guides/install-python/) for Python dependency management
2. `uv sync --frozen` to install dependencies.
3. `uv run flask run --host 0.0.0.0` to run the app.

The frontend relies on a backend API server to function. The API server is defined in the `backend` project folder.

## Pages
- Dashboard: Summary graphs of things like net worth, expenses over time, and margins
- Budget: Page that allows you to set budget allocations per expense type and to see actuals against those allocations
- Journal: Workhorse of the app. This is where you add, edit, or delete journal entries.
- Statements: This is where quarterly balance sheets, income statements, and cashflow statements can be viewed.
- Import Transactions: This page allows you to import transactions from a variety of financial institutions into the Journal
- Manage: Settings and preferences. This page lets you configure new accounts.
