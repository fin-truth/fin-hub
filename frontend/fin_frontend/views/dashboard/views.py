import json
import time

import requests
from flask import Blueprint, current_app, render_template

from fin_frontend.decorators import extends_base_html
from fin_frontend.extensions.cache import cache
from fin_frontend.utils import external_api_url_for

dashboard = Blueprint("dashboard", __name__, url_prefix="/", template_folder="templates", static_folder="static")


@dashboard.route("/")
@extends_base_html
@cache.cached(timeout=1000)
def index(base_params):
    start = time.time()

    # Call the API endpoint
    api_url = external_api_url_for("dashboard")
    response = requests.get(api_url)

    if response.status_code == 200:
        dashboard_data = response.json()
        end = time.time()
        current_app.logger.debug(f"API call took: {end - start}")

        # Deserialize JSON strings for pure data. Leave prebuilt plots as JSON strings
        net_worth_plot = json.loads(dashboard_data["net_worth"]["data"])
        operating_expenses_plot = json.loads(dashboard_data["operating_expenses"]["data"])
        asset_distribution = json.loads(dashboard_data["asset_distribution"]["data"])
        unrealized_gains_plot = json.loads(dashboard_data["unrealized_gains"]["data"])
        margins_plot = json.loads(dashboard_data["margins"]["data"])

        return render_template(
            "dashboard.html",
            net_worth_plot=net_worth_plot,
            operating_expenses_plot=operating_expenses_plot,
            asset_distribution=asset_distribution,
            unrealized_gains_plot=unrealized_gains_plot,
            margins_plot=margins_plot,
            histograms_plot=dashboard_data["histograms"]["data"],
            **base_params,
        )
    else:
        current_app.logger.error(f"API call failed with status code: {response.status_code}")
        return render_template("error.html", message="Failed to load dashboard data"), 500
