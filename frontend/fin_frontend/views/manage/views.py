import requests
from flask import Blueprint, jsonify, redirect, render_template, url_for
from webargs import fields
from webargs.flaskparser import use_kwargs

from fin_frontend.decorators import extends_base_html
from fin_frontend.forms import (
    ACCOUNT_TYPES,
    BALANCE_TYPES,
    FINANCIAL_STATEMENTS,
    build_forms,
)
from fin_frontend.utils import external_api_url_for

manage_view = Blueprint(
    "manage",
    __name__,
    url_prefix="/manage",
    template_folder="templates",
    static_folder="static",
)


@manage_view.route("")
@extends_base_html
def main(base_params):
    return render_template("manage_overview.html", **base_params)


@manage_view.route("/accounts", methods=["GET", "POST"])
@extends_base_html
def accounts(base_params):
    account_form, _ = build_forms("accounts")

    # Fetch accounts from API
    api_url = external_api_url_for("accounts")
    response = requests.get(api_url)

    if response.status_code != 200:
        return render_template("error.html", message="Failed to load accounts data"), 500

    accounts = response.json()
    accounts_table = [
        [
            account["id"],
            account["human_name"],
            account["description"],
            account["balance_type"],
            account["account_type"],
            account["financial_statement"],
            account["is_archived"],
        ]
        for account in accounts
    ]

    account_types = ACCOUNT_TYPES
    account_types = [(account_type, account_type.replace("_", " ").capitalize()) for account_type in account_types]
    balance_types = BALANCE_TYPES
    balance_types = [(balance_type, balance_type.replace("_", " ").capitalize()) for balance_type in balance_types]
    financial_statements = FINANCIAL_STATEMENTS
    financial_statements = [
        (financial_statements, financial_statements.replace("_", " ").capitalize())
        for financial_statements in financial_statements
    ]

    if account_form.submit.data and account_form.validate_on_submit():
        # Submit new account to API
        new_account_data = {
            "human_name": account_form.human_name.data,
            "description": account_form.description.data,
            "balance_type": account_form.balance_type.data,
            "account_type": account_form.account_type.data,
            "financial_statement": account_form.financial_statement.data,
        }
        create_url = external_api_url_for("accounts")
        create_response = requests.post(create_url, json=new_account_data)

        if create_response.status_code == 201:
            return redirect(url_for("manage.accounts"))
        else:
            return render_template("error.html", message="Failed to create new account"), 500

    return render_template(
        "accounts.html",
        account_form=account_form,
        accounts_table=accounts_table,
        account_types=account_types,
        balance_types=balance_types,
        financial_statements=financial_statements,
        **base_params,
    )


@manage_view.route("/_edit_account", methods=["POST"])
@use_kwargs(
    {
        "db_id": fields.Int(required=True),
        "new_name": fields.String(required=True),
        "new_description": fields.String(required=True),
        "new_balance_type": fields.String(required=True),
        "new_account_type": fields.String(required=True),
        "new_fin_statement": fields.String(required=True),
        "new_is_archived": fields.Bool(required=True),
    }
)
def _edit_row(
    db_id: int,
    new_name: str,
    new_description: str,
    new_balance_type: str,
    new_account_type: str,
    new_fin_statement: str,
    new_is_archived: bool,
):
    # Update account using API
    update_data = {
        "human_name": new_name,
        "description": new_description,
        "balance_type": new_balance_type,
        "account_type": new_account_type,
        "financial_statement": new_fin_statement,
        "is_archived": new_is_archived,
    }
    update_url = external_api_url_for("accounts")
    update_response = requests.put(update_url, json=update_data)

    if update_response.status_code == 200:
        return jsonify({"success": True, "redirect": url_for("manage.accounts")})
    else:
        return jsonify({"success": False, "message": "Failed to update account"}), 500
