$(function saveEdits() {
    $(".account-cell").on('focusout', function () {
        let tds = $(this).closest("tr").find("td");
        let id = tds.eq(0).text();
        let name = tds.eq(1).text();
        let description = tds.eq(2).text();
        let balanceType = tds.eq(3).children().first().val();
        let accountType = tds.eq(4).children().first().val();
        let finStatement = tds.eq(5).children().first().val();
        let isArchived = tds.eq(6).children().first().val();

        $.ajax({
            url: `/api/1/accounts/${id}`,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({
                db_id: id,
                human_name: name,
                description: description,
                balance_type: balanceType,
                account_type: accountType,
                financial_statement: finStatement,
                is_archived: isArchived
            }),
            success: function(response) {
                console.log('Account updated successfully:', response);
            },
            error: function(xhr, status, error) {
                console.error('Error:', error);
                alert('An error occurred while updating the account.');
            }
        });
        return false;
    });
});

