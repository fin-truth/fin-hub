from datetime import datetime, timedelta

import requests
from flask import Blueprint, current_app, redirect, render_template, url_for

from fin_frontend.decorators import extends_base_html
from fin_frontend.forms import build_forms
from fin_frontend.forms.accounts_form import SubmitForm
from fin_frontend.utils import (
    external_api_url_for,
    first_of_month,
    format_balance,
    get_next_quarter,
    get_quarter,
    prettify_flows,
)

statements_view = Blueprint(
    "statements",
    __name__,
    url_prefix="/statements",
    template_folder="templates",
    static_folder="static",
)


def get_api_data(endpoint, params=None):
    api_url = f"{external_api_url_for(endpoint)}"
    response = requests.get(api_url, params=params)
    response.raise_for_status()
    return response.json()


@statements_view.route("")
def main():
    return redirect(url_for("statements.balance_sheet"))


@statements_view.route("/balance_sheet", methods=["GET", "POST"])
@extends_base_html
def balance_sheet(base_params):
    _, quarter_form = build_forms("quarter")

    params = {}
    if quarter_form.update.data:
        params["year"] = quarter_form.year.data
        params["quarter"] = quarter_form.quarter.data

        quarter_begin = datetime(
            int(quarter_form.year.data),
            (int(quarter_form.quarter.data) - 1) * 3 + 1,
            1,
            0,
            0,
            0,
        )
        next_quarter_start = get_next_quarter(quarter_begin)
    else:
        quarter_begin, quarter_end = get_quarter()
        next_quarter_start = quarter_end + timedelta(milliseconds=2)

    balance_sheet_data = get_api_data("balance_sheet", params)

    submit_form = SubmitForm()
    submit_form.submit.label.text = "Perform Close on Date"
    submit_form.on_date.data = first_of_month()

    if submit_form.submit.data:
        close_date = submit_form.on_date.data.strftime("%Y-%m-%d")
        api_url = external_api_url_for("close")
        try:
            response = requests.post(api_url, json={"date": close_date})
            response.raise_for_status()
            return redirect(url_for("statements.balance_sheet"))
        except requests.HTTPError:
            error_message = "An error occurred while performing the close operation."
            try:
                error_data = response.json()
                if "error" in error_data:
                    error_message = error_data["error"]
            except ValueError:
                pass
            current_app.logger.error(f"HTTP error during close operation: {error_message}")

    header = (
        "Accounts",
        next_quarter_start.strftime("%B %d, %Y"),
        quarter_begin.strftime("%B %d, %Y"),
    )
    display_cash_assets = [
        (
            cash_asset["account_human_name"],
            format_balance(cash_asset["current_balance"], False),
            format_balance(cash_asset["previous_balance"], False),
        )
        for cash_asset in balance_sheet_data["assets"]["cash_assets"]
    ]
    cash_assets_total = (
        "Cash Assets",
        format_balance(sum([b["current_balance"] for b in balance_sheet_data["assets"]["cash_assets"]]), True),
        format_balance(sum([b["previous_balance"] for b in balance_sheet_data["assets"]["cash_assets"]]), True),
    )
    display_liquid_assets = [
        (
            liquid_asset["account_human_name"],
            format_balance(liquid_asset["current_balance"], False),
            format_balance(liquid_asset["previous_balance"], False),
        )
        for liquid_asset in balance_sheet_data["assets"]["noncash_liquid_assets"]
    ]
    liquid_assets_total = (
        "Noncash Liquid Assets",
        format_balance(
            sum([b["current_balance"] for b in balance_sheet_data["assets"]["noncash_liquid_assets"]]), True
        ),
        format_balance(
            sum([b["previous_balance"] for b in balance_sheet_data["assets"]["noncash_liquid_assets"]]), True
        ),
    )

    display_illiquid_assets = [
        (
            illiquid_asset["account_human_name"],
            format_balance(illiquid_asset["current_balance"], False),
            format_balance(illiquid_asset["previous_balance"], False),
        )
        for illiquid_asset in balance_sheet_data["assets"]["noncash_illiquid_assets"]
    ]
    illiquid_assets_total = (
        "Noncash Illiquid Assets",
        format_balance(
            sum([b["current_balance"] for b in balance_sheet_data["assets"]["noncash_illiquid_assets"]]), True
        ),
        format_balance(
            sum([b["previous_balance"] for b in balance_sheet_data["assets"]["noncash_illiquid_assets"]]), True
        ),
    )

    display_contra_assets = [
        (
            contra_asset["account_human_name"],
            format_balance(contra_asset["current_balance"], False),
            format_balance(contra_asset["previous_balance"], False),
        )
        for contra_asset in balance_sheet_data["assets"]["contra_assets"]
    ]
    contra_assets_total = (
        "Contra Assets",
        format_balance(sum([b["current_balance"] for b in balance_sheet_data["assets"]["contra_assets"]]), True),
        format_balance(sum([b["previous_balance"] for b in balance_sheet_data["assets"]["contra_assets"]]), True),
    )

    assets_total = (
        format_balance(balance_sheet_data["assets"]["total"]),
        format_balance(balance_sheet_data["assets"]["previous_total"]),
    )

    display_longterms = [
        (
            longterm_liability["account_human_name"],
            format_balance(longterm_liability["current_balance"], False),
            format_balance(longterm_liability["previous_balance"], False),
        )
        for longterm_liability in balance_sheet_data["liabilities"]["longterm_liabilities"]
    ]
    longterms_total = (
        "Longterm Liabilities",
        format_balance(
            sum([b["current_balance"] for b in balance_sheet_data["liabilities"]["longterm_liabilities"]]), True
        ),
        format_balance(
            sum([b["previous_balance"] for b in balance_sheet_data["liabilities"]["longterm_liabilities"]]), True
        ),
    )
    display_revolving = [
        (
            revolving_liability["account_human_name"],
            format_balance(revolving_liability["current_balance"], False),
            format_balance(revolving_liability["previous_balance"], False),
        )
        for revolving_liability in balance_sheet_data["liabilities"]["revolving_liabilities"]
    ]
    revolving_total = (
        "Revolving Liabilities",
        format_balance(
            sum([b["current_balance"] for b in balance_sheet_data["liabilities"]["revolving_liabilities"]]), True
        ),
        format_balance(
            sum([b["previous_balance"] for b in balance_sheet_data["liabilities"]["revolving_liabilities"]]), True
        ),
    )
    liabilities_total = (
        format_balance(balance_sheet_data["liabilities"]["total"]),
        format_balance(balance_sheet_data["liabilities"]["previous_total"]),
    )

    display_equities = [
        (
            equities["account_human_name"],
            format_balance(equities["current_balance"], False),
            format_balance(equities["previous_balance"], False),
        )
        for equities in balance_sheet_data["equity"]["accounts"]
    ]
    equity_total = (
        "Equity",
        format_balance(balance_sheet_data["equity"]["total"], True),
        format_balance(balance_sheet_data["equity"]["previous_total"], True),
    )

    equity_liabilities_total = (
        format_balance(balance_sheet_data["total_liabilities_and_equity"], True),
        format_balance(balance_sheet_data["total_previous_liabilities_and_equity"], True),
    )

    return render_template(
        "balance_sheet.html",
        header=header,
        quarter_form=quarter_form,
        display_cash_assets=display_cash_assets,
        cash_assets_total=cash_assets_total,
        display_liquid_assets=display_liquid_assets,
        liquid_assets_total=liquid_assets_total,
        display_illiquid_assets=display_illiquid_assets,
        illiquid_assets_total=illiquid_assets_total,
        display_contra_assets=display_contra_assets,
        contra_assets_total=contra_assets_total,
        total_assets=assets_total,
        display_longterm_liabilities=display_longterms,
        longterm_totals=longterms_total,
        display_revolving_liabilities=display_revolving,
        revolving_totals=revolving_total,
        total_liabilities=liabilities_total,
        display_equity=display_equities,
        equity_total=equity_total,
        equity_liab_total=equity_liabilities_total,
        submit_form=submit_form,
        **base_params,
    )


@statements_view.route("/income_statement", methods=["GET", "POST"])
@extends_base_html
def income_statement(base_params):
    _, quarter_form = build_forms("quarter")

    params = {}
    if quarter_form.update.data:
        params["year"] = quarter_form.year.data
        params["quarter"] = quarter_form.quarter.data

    statement = get_api_data("income_statement", params)

    # Gather everything for display
    revenues = [[name, format_balance(balance)] for name, balance in statement["revenues"].items()]
    gross_revenue = ["Gross Revenue", format_balance(statement["total_revenue"])]

    raw_op_expenses = statement["operating_expenses"]
    if len(raw_op_expenses) > 0:
        op_expenses = [[name, format_balance(-1 * abs(balance))] for name, balance in raw_op_expenses.items()]
    else:
        op_expenses = None

    op_profit = ["Operating Profit", format_balance(statement["operating_profit"])]

    raw_gains = statement["gains"]
    if len(raw_gains) > 0:
        display_gains = [[name, format_balance(balance)] for name, balance in raw_gains.items()]
    else:
        display_gains = None

    pre_tax_income = ["Income Before Tax", format_balance(statement["income_pre_tax"])]

    raw_cont_op_expenses = statement["continued_operation_expenses"]
    if len(raw_cont_op_expenses) > 0:
        cont_op_expenses = [[name, format_balance(balance)] for name, balance in raw_cont_op_expenses.items()]
    else:
        cont_op_expenses = None

    income = ["Income", format_balance(statement["net_income"])]

    return render_template(
        "income_statement.html",
        revenues=revenues,
        gross_revenue=gross_revenue,
        op_expenses=op_expenses,
        op_profit=op_profit,
        gains=display_gains,
        pre_tax_income=pre_tax_income,
        quarter_form=quarter_form,
        cont_op_expenses=cont_op_expenses,
        income=income,
        **base_params,
    )


@statements_view.route("/cash_flow_statement", methods=["GET", "POST"])
@extends_base_html
def cash_flow_statement(base_params):
    _, quarter_form = build_forms("quarter")

    params = {}
    if quarter_form.update.data:
        params["year"] = quarter_form.year.data
        params["quarter"] = quarter_form.quarter.data

    cash_flow = get_api_data("cash_flow", params)

    operating_activities = prettify_flows(cash_flow["operating_flows"])
    net_operating_activities = [
        "Net Cash from Operations",
        format_balance(cash_flow["flow_from_ops"], False),
    ]

    investment_activities = prettify_flows(cash_flow["investment_flows"])
    net_investment_activities = [
        "Net Cash from Investments",
        format_balance(cash_flow["flow_from_investments"], False),
    ]

    financing_activities = prettify_flows(cash_flow["financing_flows"])
    net_financing_activities = [
        "Net Cash from Financing",
        format_balance(cash_flow["flow_from_financing"], False),
    ]

    net_cash = [
        ["Net Cash Flows", format_balance(cash_flow["total_cash_flow"], False)],
        ["True Difference", format_balance(cash_flow["true_difference"], False)],
        ["Initial Cash Balance", format_balance(cash_flow["initial_cash_balance"], False)],
        ["Final Cash Balance", format_balance(cash_flow["final_cash_balance"], False)],
    ]

    return render_template(
        "cash_flow_statement.html",
        operating_activities=operating_activities,
        net_operating_activities=net_operating_activities,
        quarter_form=quarter_form,
        investment_activities=investment_activities,
        net_investment_activities=net_investment_activities,
        financing_activities=financing_activities,
        net_financing_activities=net_financing_activities,
        net_cash=net_cash,
        **base_params,
    )
