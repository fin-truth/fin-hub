import io
import logging

import requests
from flask import Blueprint, redirect, render_template, url_for
from werkzeug.datastructures import FileStorage

from fin_frontend.decorators import extends_base_html
from fin_frontend.forms.import_form import ImportForm
from fin_frontend.utils import external_api_url_for, get_displayable_accounts

import_view = Blueprint(
    "import",
    __name__,
    url_prefix="/import",
    template_folder="templates",
    static_folder="static",
)

logger = logging.getLogger(__name__)


def get_supported_providers():
    api_url = external_api_url_for("providers")
    try:
        response = requests.get(api_url)
        response.raise_for_status()
        return response.json()
    except requests.RequestException as e:
        logger.error(f"Error fetching supported providers: {str(e)}")
        return []


@import_view.route("", methods=["GET", "POST"])
@extends_base_html
def main(base_params):
    form = ImportForm()
    form.account_id.choices = get_displayable_accounts(
        account_types=["cash_asset", "noncash_liquid_asset", "noncash_illiquid_asset", "revolving_liability"]
    )
    form.revenue_account_id.choices = get_displayable_accounts(account_types=["revenue"])
    form.provider.choices = [(provider, provider) for provider in get_supported_providers()]

    if form.validate_on_submit():
        file = form.uploaded_file.data
        file_storage = FileStorage(
            stream=io.BytesIO(file.read()), filename=file.filename, content_type=file.content_type
        )

        data = {
            "provider": form.provider.data,
            "account_id": form.account_id.data,
            "revenue_account_id": form.revenue_account_id.data,
        }

        files = {"file": file_storage}

        api_url = external_api_url_for("import")

        try:
            response = requests.post(api_url, data=data, files=files)
            response.raise_for_status()

            return redirect(url_for("import.success"))
        except requests.RequestException as e:
            logger.error(f"Error importing file: {str(e)}")
            error_message = "An error occurred while importing the file. Please try again."
            return render_template("import_files.html", form=form, error_message=error_message, **base_params)

    return render_template("import_files.html", form=form, **base_params)


@import_view.route("/success", methods=["GET"])
@extends_base_html
def success(base_params):
    return render_template("success.html", **base_params)
