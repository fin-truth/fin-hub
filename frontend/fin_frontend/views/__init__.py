import requests
from flask import Blueprint, current_app, request

from fin_frontend.views.budget.views import budget_view
from fin_frontend.views.dashboard.views import dashboard
from fin_frontend.views.import_files.views import import_view
from fin_frontend.views.journal.views import journal_view
from fin_frontend.views.manage.views import manage_view
from fin_frontend.views.statements.views import statements_view

api_proxy = Blueprint("api_proxy", __name__)


@api_proxy.route("/api/<path:path>", methods=["GET", "POST", "PUT", "DELETE"])
def proxy(path):
    api_url = f"{current_app.config['API_BASE_URL']}/api/{path}"
    resp = requests.request(
        method=request.method,
        url=api_url,
        headers={key: value for (key, value) in request.headers if key != "Host"},
        data=request.get_data(),
        cookies=request.cookies,
        allow_redirects=False,
    )
    excluded_headers = ["content-encoding", "content-length", "transfer-encoding", "connection"]
    headers = [(name, value) for (name, value) in resp.raw.headers.items() if name.lower() not in excluded_headers]
    response = current_app.response_class(resp.content, resp.status_code, headers)
    return response
