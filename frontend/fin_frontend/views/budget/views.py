import requests
from flask import Blueprint, current_app, redirect, render_template, request, url_for

from fin_frontend.decorators import extends_base_html
from fin_frontend.forms import build_forms
from fin_frontend.utils import external_api_url_for, first_of_month, last_of_month

budget_view = Blueprint(
    "budget",
    __name__,
    url_prefix="/budget",
    template_folder="templates",
    static_folder="static",
)


@budget_view.route("", methods=["GET", "POST"])
@extends_base_html
def main(base_params):
    _, range_form = build_forms("budget", first_of_month(), last_of_month())

    if range_form.update.data and range_form.validate():
        return redirect(
            url_for(
                "budget.main",
                from_date=range_form.begin_date.data,
                to_date=range_form.end_date.data,
            )
        )

    from_date = request.args.get("from_date", first_of_month().strftime("%Y-%m-%d"))
    to_date = request.args.get("to_date", last_of_month().strftime("%Y-%m-%d"))

    # Call the API to get budget data
    api_url = external_api_url_for("budget")
    response = requests.get(api_url, params={"from_date": from_date, "to_date": to_date, "include_misc": "true"})

    if response.status_code == 200:
        budget_data = response.json()
        main_categories = [account for account in budget_data if account["account_type"] == "expense"]
        misc_categories = [account for account in budget_data if account["account_type"] == "misc_expense"]
    else:
        current_app.logger.error(f"Failed to fetch budget data. Status code: {response.status_code}")
        budget_data = []

    return render_template(
        "budget_overview.html",
        range_form=range_form,
        from_date=from_date,
        to_date=to_date,
        main_categories=main_categories,
        misc_categories=misc_categories,
        **base_params,
    )
