$(function() {
    // Function to format currency
    function formatCurrency(amount) {
        return '$' + parseFloat(amount).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
    }

    // Function to update allotment
    function updateAllotment(id, newAllotment) {
        $.ajax({
            url: '/api/1/budget/allotment',
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify({
                account_id: id,
                new_allotment: newAllotment
            }),
            success: function(response) {
                if (response.success) {
                    console.log('Allotment updated successfully');
                    // Update the balance cell
                    updateBalance(id, newAllotment);
                } else {
                    console.error('Failed to update allotment');
                    // Revert the cell to its original value
                    location.reload();
                }
            },
            error: function(xhr, status, error) {
                console.error('Error updating allotment:', error);
                // Revert the cell to its original value
                location.reload();
            }
        });
    }

    // Function to update balance
    function updateBalance(id, newAllotment) {
        var row = $(`td.allotment-id:contains(${id})`).closest('tr');
        var softTotalCell = row.find('td:nth-child(5)');
        var balanceCell = row.find('td:nth-child(6)');
        
        var softTotal = parseFloat(softTotalCell.text().replace(/[$,]/g, ''));
        var newBalance = newAllotment - softTotal;
        
        balanceCell.text(formatCurrency(newBalance));
    }

    // Event listener for focusout on allotment cells
    $(".allotment-cell").on('focusout', function() {
        var id = $(this).closest("tr").find(".allotment-id").text();
        var newAllotment = $(this).text().replace(/[$,]/g, '');
        
        // Validate input
        if (isNaN(newAllotment) || newAllotment === "") {
            alert("Please enter a valid number for the allotment.");
            location.reload();
            return;
        }
        
        newAllotment = parseFloat(newAllotment);
        
        // Format the cell value
        $(this).text(formatCurrency(newAllotment));
        
        // Update the allotment
        updateAllotment(id, newAllotment);
    });

    // Event listener for keypress on allotment cells
    $(".allotment-cell").on('keypress', function(e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
});

