import csv
import logging
from datetime import datetime, timedelta

import requests
from flask import Blueprint, current_app, redirect, render_template, send_file, url_for
from webargs import fields
from webargs.flaskparser import use_kwargs

from fin_frontend.decorators import extends_base_html
from fin_frontend.forms import build_forms
from fin_frontend.utils import external_api_url_for, first_of_month, last_of_month

journal_view = Blueprint(
    "journal",
    __name__,
    url_prefix="/journal",
    template_folder="templates",
    static_folder="static",
)


@journal_view.app_template_filter("currency")
def currency_filter(value):
    return f"${value:.2f}"


logger = logging.getLogger(__name__)


@journal_view.route("", methods=["GET", "POST"])
@extends_base_html
@use_kwargs(
    {
        "from_date": fields.Date(required=False, format="%Y-%m-%d"),
        "to_date": fields.Date(required=False, format="%Y-%m-%d"),
        "account_id": fields.Int(required=False),
    },
    location="query",
)
def main(
    base_params,
    from_date: datetime = first_of_month() - timedelta(days=30),
    to_date: datetime = last_of_month(),
    account_id: int = 0,
):
    # Build forms
    journal_form, range_form = build_forms("journal", from_date, to_date)

    # Update range of items if requested
    if range_form.update.data and range_form.validate():
        return redirect(
            url_for(
                "journal.main",
                from_date=range_form.begin_date.data,
                to_date=range_form.end_date.data,
                account_id=range_form.account_id.data,
            )
        )

    # Fetch journal entries from API
    api_url = external_api_url_for("journal")
    params = {
        "from_date": from_date.strftime("%Y-%m-%d"),
        "to_date": to_date.strftime("%Y-%m-%d"),
    }
    if account_id:
        params["account_id"] = account_id

    response = requests.get(api_url, params=params)
    if response.status_code == 200:
        journals = response.json()
    else:
        journals = []
        logger.error(f"Failed to fetch journal entries: {response.text}")

    # Export CSV if requested
    if range_form.export_selected.data and range_form.validate():
        account_id = int(range_form.account_id.data)
        if account_id == 0:
            account_name = "All Accounts"
            export_account_id = 0
            account_beginning = "N/A"
            account_end = "N/A"
        else:
            api_url = external_api_url_for("accounts")
            response = requests.get(f"{api_url}/{account_id}")
            if response.status_code == 200:
                account = response.json()
            else:
                logger.error(f"Failed to fetch account {account_id}: {response.text}")
                return render_template("error.html", message="Failed to export journal entries"), 500
            account_name = account["human_name"]
            export_account_id = account["id"]
            # Get account balances from API
            balances_url = external_api_url_for("balances")

            # Get beginning balance
            begin_params = {"account_id": account_id, "up_to_date": range_form.begin_date.data.strftime("%Y-%m-%d")}
            begin_response = requests.get(balances_url, params=begin_params)
            if begin_response.status_code != 200:
                logger.error(f"Failed to fetch beginning balance: {begin_response.text}")
                return render_template("error.html", message="Failed to export journal entries"), 500
            account_beginning = round(begin_response.json()[0]["balance"], 2)

            # Get ending balance
            end_params = {"account_id": account_id, "up_to_date": range_form.end_date.data.strftime("%Y-%m-%d")}
            end_response = requests.get(balances_url, params=end_params)
            if end_response.status_code != 200:
                logger.error(f"Failed to fetch ending balance: {end_response.text}")
                return render_template("error.html", message="Failed to export journal entries"), 500
            account_end = round(end_response.json()[0]["balance"], 2)
        file_path = f'/tmp/{account_name}_journals_{datetime.strftime(datetime.now(), format="%Y-%m-%d_%H-%M")}.csv'
        with open(file_path, "w", newline="") as csvfile:
            writer = csv.writer(csvfile, delimiter=",")
            writer.writerow(["account_id", "account_name", "begin_date", "begin_balance", "end_date", "end_balance"])
            writer.writerow(
                [
                    export_account_id,
                    account_name,
                    datetime.strftime(range_form.begin_date.data, format="%Y-%m-%d %H:%M"),
                    account_beginning,
                    datetime.strftime(range_form.end_date.data, format="%Y-%m-%d %H:%M"),
                    account_end,
                ]
            )
            writer.writerow([])
            writer.writerow(
                [
                    "journal_id",
                    "date",
                    "description",
                    "debited_account",
                    "debited_amount",
                    "credited_account",
                    "credited_amount",
                ]
            )
            for row in journals:
                writer.writerow(row.values())
        return send_file(file_path, as_attachment=True)

    # Update range form defaults
    range_form.account_id.default = account_id
    range_form.process()
    range_form.begin_date.data = from_date
    range_form.end_date.data = to_date

    # Handle form submission for new journal entry
    if journal_form.submit.data and journal_form.validate():
        new_entry_data = {
            "timestamp": journal_form.timestamp.data.strftime("%Y-%m-%d %H:%M"),
            "description": journal_form.description.data,
            "credited_account_id": journal_form.credited_account_id.data,
            "credited_amount": float(journal_form.credited_amount.data),
            "debited_account_id": journal_form.debited_account_id.data,
            "debited_amount": float(journal_form.debited_amount.data),
        }
        response = requests.post(api_url, json=new_entry_data)
        if response.status_code == 201:
            return redirect(
                url_for(
                    "journal.main",
                    from_date=from_date,
                    to_date=to_date,
                    account_id=account_id,
                )
            )
        else:
            logger.error(f"Failed to create new journal entry: {response.text}")

    return render_template(
        "journal.html",
        title="Journal",
        range_form=range_form,
        journals=journals,
        account_options=journal_form.debited_account_id.choices,
        current_to_date=to_date,
        current_from_date=from_date,
        journal_form=journal_form,
        current_account_id=account_id,
        api_base_url=current_app.config["API_BASE_URL"],
        **base_params,
    )
