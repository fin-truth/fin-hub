$(function() {
    function formatCurrency(amount) {
        return new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(amount);
    }
    function parseCurrency(value) {
        return parseFloat(value.replace(/[^0-9.-]+/g,""));
    }

    function updateJournalEntry(row) {
        let top_row, bottom_row;
        if (row.hasClass("journal-top-row")) {
            top_row = row;
            bottom_row = row.next("tr");
        } else {
            top_row = row.prev("tr");
            bottom_row = row;
        }

        let data = {
            db_id: top_row.find("td:first").text(),
            timestamp: top_row.find('input.flatpickr').val(),
            description: top_row.find("td:nth-child(3)").text(),
            debited_account_id: top_row.find("td:nth-child(4) select").val(),
            debited_amount: parseCurrency(top_row.find("td:nth-child(5)").text()),
            credited_account_id: bottom_row.find("td:first select").val(),
            credited_amount: parseCurrency(bottom_row.find("td:nth-child(2)").text())
        };
        $.ajax({
            url: `/api/1/journal/${data.db_id}`,
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(data),
            success: function(response) {
                console.log('Journal entry updated successfully');
                // Optionally, update the UI here if needed
                // Update the displayed values with formatted currency
                top_row.find("td:nth-child(5)").text(formatCurrency(data.debited_amount));
                bottom_row.find("td:nth-child(2)").text(formatCurrency(data.credited_amount));
            },
            error: function(xhr, status, error) {
                console.error('Error updating journal entry:', error);
                alert('Failed to update journal entry. Please try again.');
            }
        });
    }

    // Initialize Flatpickr
    flatpickr(".flatpickr", {
        enableTime: true,
        dateFormat: "Y-m-d H:i",
        time_24hr: true,
        defaultHour: new Date().getHours(),
        defaultMinute: new Date().getMinutes(),
        minuteIncrement: 15
    });

    $(".journal-cell").on('focusout', function () {
        let row = $(this).closest("tr");

        updateJournalEntry(row);
    });

    $(".delete-journal-entry").on('click', function(e) {
        e.preventDefault();
        if (confirm('Are you sure you want to delete this journal entry?')) {
            let entryId = $(this).data('entry-id');
            $.ajax({
                url: '/api/1/journal/' + entryId,
                type: 'DELETE',
                success: function(response) {
                    console.log('Journal entry deleted successfully');
                    // Remove the entry from the UI
                    $(e.target).closest('tr').next('tr').remove();
                    $(e.target).closest('tr').remove();
                },
                error: function(xhr, status, error) {
                    console.error('Error deleting journal entry:', error);
                    alert('Failed to delete journal entry. Please try again.');
                }
            });
        }
    });
});
