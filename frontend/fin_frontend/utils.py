import math
from datetime import date, datetime, timedelta
from typing import Dict, List, Tuple

import requests
from flask import current_app


def external_api_url_for(endpoint):
    """Generate an external URL for the API based on the current environment."""
    api_base_url = current_app.config["API_BASE_URL"]
    api_endpoints = current_app.config["API_ENDPOINTS"]

    if endpoint not in api_endpoints:
        raise ValueError(f"Unknown API endpoint: {endpoint}")

    return f"{api_base_url}{api_endpoints[endpoint]}"


def first_of_month() -> date:
    today = date.today()
    return date(today.year, today.month, 1)


def last_of_month() -> date:
    today = date.today()
    if (int(today.month) + 1) % 12 < int(today.month):
        first_next_month = date(today.year + 1, 1, 1)
    else:
        first_next_month = date(today.year, (today.month + 1) % 12, 1)
    delta = timedelta(hours=1)
    return first_next_month - delta


def get_displayable_accounts(include_archived: bool = False, account_types: list = None):
    api_url = external_api_url_for("accounts")
    params = {"include_archived": str(include_archived).lower()}

    if account_types:
        params["account_types"] = account_types

    try:
        response = requests.get(api_url, params=params)
        response.raise_for_status()
        accounts = response.json()
        return [
            (
                str(account["id"]),
                account["human_name"] if not account["is_archived"] else f"Archived: {account['human_name']}",
            )
            for account in accounts
        ]
    except requests.RequestException as e:
        current_app.logger.error(f"Error fetching accounts from API: {str(e)}")
        return []


def format_balance(balance: float, include_dollar: bool = False) -> str:
    """Takes an AccountBalance and returns a printable string."""
    if round(balance, 2) >= 0:
        formatted_balance = f"{abs(balance):,.2f} "
    else:
        formatted_balance = f"({abs(balance):,.2f})"

    if include_dollar:
        formatted_balance = f"${formatted_balance}"
    return formatted_balance


def prettify_flows(cash_flows: Dict[Dict, float]) -> List[Tuple[str, str]]:
    printable_flows = []
    for cash_flow in cash_flows:
        if not math.isclose(cash_flow["change_in_quarter"], 0, abs_tol=1e-4):
            if cash_flow["increases_cash_flow"]:
                printable_flows.append(
                    (
                        f"Increase due to increase in {cash_flow['account_human_name']}",
                        format_balance(cash_flow["change_in_quarter"], False),
                    )
                )
            else:
                printable_flows.append(
                    (
                        f"Decrease due to decrease in {cash_flow['account_human_name']}",
                        format_balance(-1.0 * cash_flow["change_in_quarter"], False),
                    )
                )
    return printable_flows


def get_next_quarter(quarter: datetime) -> datetime:
    current_quarter = math.ceil(quarter.month / 3)
    next_quarter = current_quarter + 1 if current_quarter != 4 else 1
    month_next_quarter = (next_quarter - 1) * 3 + 1
    year = quarter.year if current_quarter != 4 else quarter.year + 1
    return datetime(year, month_next_quarter, 1, 0, 0, 0)


def get_quarter() -> Tuple[datetime, datetime]:
    """Returns beginning of current quarter, and beginning of next quarter"""
    today = date.today()
    quarters = {
        1: datetime(today.year, 1, 1, 0, 0, 0),
        2: datetime(today.year, 4, 1, 0, 0, 0),
        3: datetime(today.year, 7, 1, 0, 0, 0),
        4: datetime(today.year, 10, 1, 0, 0, 0),
        5: datetime(today.year + 1, 1, 1, 0, 0, 0),
    }
    month = today.month
    quarter = math.ceil(month / 3)
    return quarters[quarter], quarters[quarter + 1] - timedelta(milliseconds=1)
