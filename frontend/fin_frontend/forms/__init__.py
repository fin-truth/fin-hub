import math
from datetime import date

from fin_frontend.forms.accounts_form import AccountsForm
from fin_frontend.forms.itemized_forms import (
    BaseSelectForm,
    JournalRangeSelectForm,
    QuarterSelectForm,
)
from fin_frontend.forms.journal_form import JournalForm
from fin_frontend.utils import get_displayable_accounts

ACCOUNT_TYPES = [
    "cash_asset",
    "noncash_liquid_asset",
    "noncash_illiquid_asset",
    "equity",
    "revolving_liability",
    "longterm_liability",
    "expense",
    "misc_expense",
    "contra_asset",
    "revenue",
]

BALANCE_TYPES = ["debit", "credit"]

FINANCIAL_STATEMENTS = ["balance_sheet", "income_statement"]


def build_forms(form_type: str, from_date: date = None, to_date: date = None):
    if form_type == "journal":
        item_form = JournalForm()
        displayable_accounts = get_displayable_accounts(include_archived=True)
        item_form.credited_account_id.choices = displayable_accounts
        item_form.debited_account_id.choices = displayable_accounts
        # Build form that customizes date ranges
        range_form = JournalRangeSelectForm(begin_date=from_date, end_date=to_date)
        range_form.account_id.choices = [("0", "All Accounts")] + displayable_accounts

    elif form_type == "accounts":
        item_form = AccountsForm()
        item_form.balance_type.choices = [("debit", "Debit"), ("credit", "Credit")]
        item_form.account_type.choices = [
            (account_type, account_type.replace("_", " ").capitalize()) for account_type in ACCOUNT_TYPES
        ]
        item_form.financial_statement.choices = [
            ("balance_sheet", "Balance Sheet"),
            ("income_statement", "Income Statement"),
        ]
        range_form = None

    elif form_type == "budget":
        item_form = None
        range_form = BaseSelectForm(begin_date=from_date, end_date=to_date)

    elif form_type == "quarter":
        item_form = None
        today = date.today()
        range_form = QuarterSelectForm(year=today.year, quarter=math.ceil(today.month / 3))
        range_form.year.choices = [(year, year) for year in range(today.year, today.year - 7, -1)]
        range_form.quarter.choices = [(i, f"Q{i}") for i in range(1, 5)]

    else:
        raise NotImplementedError("Only journal and accounts form types are supported by this method.")
    return item_form, range_form
