from flask_wtf import FlaskForm
from wtforms import DateField, SelectField, StringField, SubmitField
from wtforms.validators import DataRequired


class AccountsForm(FlaskForm):
    human_name = StringField("Human Name", validators=[DataRequired()])
    description = StringField("Description", validators=[DataRequired()])
    balance_type = SelectField("Balance Type")
    account_type = SelectField("Account Type")
    financial_statement = SelectField("Financial Statement")
    submit = SubmitField("Submit Item")


class SubmitForm(FlaskForm):
    submit = SubmitField()
    on_date = DateField("Date", validators=[DataRequired()], format="%Y-%m-%d")
