from flask_wtf import FlaskForm
from wtforms import DateField, SelectField, SubmitField
from wtforms.validators import DataRequired


class BaseSelectForm(FlaskForm):
    begin_date = DateField("Begin Date", validators=[DataRequired()], format="%Y-%m-%d")
    end_date = DateField("End Date", validators=[DataRequired()], format="%Y-%m-%d")
    update = SubmitField("Load")


class JournalRangeSelectForm(BaseSelectForm):
    account_id = SelectField("Account Credited or Debited")
    export_selected = SubmitField("Export Selected")


class QuarterSelectForm(FlaskForm):
    year = SelectField("Year")
    quarter = SelectField("Quarter")
    update = SubmitField("Load")
