from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired
from wtforms import SelectField, SubmitField


class ImportForm(FlaskForm):
    account_id = SelectField("Select Account")
    revenue_account_id = SelectField("Select Revenue Account for Debits")
    provider = SelectField("Select Financial Provider")
    uploaded_file = FileField("File to Import", validators=[FileRequired()])
    submit = SubmitField("Import")
