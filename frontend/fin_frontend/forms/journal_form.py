from flask_wtf import FlaskForm
from wtforms import DateTimeField, DecimalField, SelectField, StringField, SubmitField
from wtforms.validators import DataRequired, NumberRange


class JournalForm(FlaskForm):
    timestamp = DateTimeField("Date", validators=[DataRequired()], format="%Y-%m-%d %H:%M")
    debited_account_id = SelectField("Debited Account")
    debited_amount = DecimalField("Debited Amount", validators=[DataRequired(), NumberRange(min=0)])
    credited_account_id = SelectField("Credited Account")
    credited_amount = DecimalField("Credited Amount", validators=[DataRequired(), NumberRange(min=0)])
    description = StringField("Description", validators=[DataRequired()])
    submit = SubmitField("Submit Item")
