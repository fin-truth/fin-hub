from flask import Flask
from flask_cors import CORS

from fin_frontend.config import config_factory
from fin_frontend.extensions.cache import init_cache
from fin_frontend.views import (
    api_proxy,
    budget_view,
    dashboard,
    import_view,
    journal_view,
    manage_view,
    statements_view,
)


def create_app(environment: str = "DEVELOPMENT"):
    config = config_factory(environment)
    app = Flask(__name__)
    CORS(app, resources={r"/api/*": {"origins": "*"}})

    app.config.from_object(config)
    init_cache(app)

    app.register_blueprint(dashboard)
    app.register_blueprint(budget_view)
    app.register_blueprint(import_view)
    app.register_blueprint(journal_view)
    app.register_blueprint(manage_view)
    app.register_blueprint(statements_view)
    app.register_blueprint(api_proxy)

    return app


if __name__ == "__main__":
    app = create_app()
    app.run(host="0.0.0.0", port=5000)
