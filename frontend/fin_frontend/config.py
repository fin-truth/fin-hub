import os

from flask import Config

APP_ROOT = os.path.dirname(os.path.dirname(__file__))


def config_factory(environment: str):
    if environment == "DEVELOPMENT":
        return DevConfig
    elif environment == "PRODUCTION":
        return ProdConfig
    elif environment == "TESTING":
        return TestConfig
    else:
        raise NotImplementedError(f"Environment {environment} is not supported!")


class ProdConfig(Config):
    SECRET_KEY = os.environ.get("SECRET_KEY") or "a-key"
    TITLE_APPENDAGE = ""
    CACHE_TYPE = "redis"
    CACHE_REDIS_URL = os.environ.get("REDIS_URL", "redis://localhost:6379/2")
    CACHE_DEFAULT_TIMEOUT = 300
    API_BASE_URL = "http://fin-api:8080"
    API_ENDPOINTS = {
        "dashboard": "/api/1/dashboard",
        "journal": "/api/1/journal",
        "budget": "/api/1/budget",
        "balance_sheet": "/api/1/statements/balance-sheet",
        "income_statement": "/api/1/statements/income-statement",
        "cash_flow": "/api/1/statements/cash-flow",
        "close": "/api/1/statements/close",
        "accounts": "/api/1/accounts",
        "balances": "/api/1/accounts/balances",
        "import": "/api/1/import",
        "providers": "/api/1/import/providers",
    }


class DevConfig(ProdConfig):
    TITLE_APPENDAGE = " Dev"
    API_BASE_URL = "http://fin-api-dev:8008"


class TestConfig(ProdConfig):
    TITLE_APPENDAGE = " Test"
    API_BASE_URL = "http://fin-api-dev:8008"
