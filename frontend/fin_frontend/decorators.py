import functools
from typing import Callable

import requests
from flask import current_app

from fin_frontend.utils import external_api_url_for, format_balance


def extends_base_html(func: Callable) -> Callable:
    """Decorator that calls wrapped function with arguments required from base.html."""

    @functools.wraps(func)
    def wrapper_extends_base_html(*args, **kwargs):
        # Call the API endpoint to get base data
        api_url = external_api_url_for("balances")
        response = requests.get(api_url)

        if response.status_code == 200:
            base_data = response.json()
            pass

            expense_accounts_balances = [
                {"account": item["account"], "balance": format_balance(item["balance"])}
                for item in base_data
                if item["account"]["account_type"] in ["expense", "misc_expense"]
            ]
            standard_accounts_balances = [
                {"account": item["account"], "balance": format_balance(item["balance"])}
                for item in base_data
                if item["account"]["account_type"] not in ["expense", "misc_expense"]
            ]

            base_params = {
                "accounts_balances": standard_accounts_balances,
                "expense_accounts_balances": expense_accounts_balances,
                "title_appendage": current_app.config["TITLE_APPENDAGE"],
            }
            value = func(*args, **kwargs, base_params=base_params)
            return value
        else:
            current_app.logger.error(f"Failed to fetch base data from API. Status code: {response.status_code}")
            # You might want to handle this error case, perhaps by returning an error page
            return "Error: Failed to load base data", 500

    return wrapper_extends_base_html
