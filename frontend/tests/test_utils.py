from fin_frontend.utils import format_balance


def test_format_balance():
    # Test positive balance with dollar sign
    assert format_balance(1234.56, True) == "$1,234.56 "

    # Test negative balance with dollar sign
    assert format_balance(-1234.56, True) == "$(1,234.56)"

    # Test positive balance without dollar sign
    assert format_balance(1234.56, False) == "1,234.56 "

    # Test negative balance without dollar sign
    assert format_balance(-1234.56, False) == "(1,234.56)"

    # Test zero balance
    assert format_balance(0, True) == "$0.00 "
