import json
from datetime import datetime
from unittest.mock import MagicMock, patch

from fin_api.lib.plots import (
    asset_distribution_plot,
    get_first_of_months,
    get_last_of_months,
    histograms_plot,
    margins_plot,
    net_worth_plot,
    operating_expenses_plot,
    unrealized_gains_over_time,
)


def test_net_worth_plot(mock_balance_sheet):
    with (
        patch("fin_api.lib.plots.get_first_of_months") as mock_get_months,
        patch("fin_api.lib.plots.BalanceSheet") as mock_balance_sheet_class,
    ):
        mock_get_months.return_value = [datetime(2023, i, 1) for i in range(1, 12)]
        mock_balance_sheet_class.return_value = mock_balance_sheet

        result = net_worth_plot()
        data = json.loads(result)

        assert isinstance(data, list)
        assert len(data) == 1
        assert data[0]["mode"] == "lines+markers"
        assert len(data[0]["x"]) == 11
        assert len(data[0]["y"]) == 11
        assert all(y == 500 for y in data[0]["y"])


def test_unrealized_gains_over_time(mock_balance_sheet):
    with (
        patch("fin_api.lib.plots.get_quarter") as mock_get_quarter,
        patch("fin_api.lib.plots.get_prev_quarter") as mock_get_prev_quarter,
        patch("fin_api.lib.plots.BalanceSheet") as mock_balance_sheet_class,
    ):
        mock_get_quarter.return_value = [datetime(2023, 3, 31)]
        prev_quarters_2022 = [datetime(2022, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2021 = [datetime(2021, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2020 = [datetime(2020, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2019 = [datetime(2019, i, 1) for i in range(12, 0, -3)]
        mock_get_prev_quarter.side_effect = (
            prev_quarters_2022 + prev_quarters_2021 + prev_quarters_2020 + prev_quarters_2019
        )
        mock_balance_sheet_class.return_value = mock_balance_sheet

        result = unrealized_gains_over_time()
        data = json.loads(result)

        # Test only verifies it runs. Mock data to do more
        assert isinstance(data, list)
        assert len(data) == 2
        assert data[0]["name"] == "Other Comp Income"
        assert data[1]["name"] == "Retained Earnings"


def test_margins_plot(mock_income_statement):
    with (
        patch("fin_api.lib.plots.get_quarter") as mock_get_quarter,
        patch("fin_api.lib.plots.get_prev_quarter") as mock_get_prev_quarter,
        patch("fin_api.lib.plots.IncomeStatement") as mock_income_statement_class,
    ):
        mock_get_quarter.return_value = [datetime(2023, 3, 31)]
        prev_quarters_2022 = [datetime(2022, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2021 = [datetime(2021, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2020 = [datetime(2020, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2019 = [datetime(2019, i, 1) for i in range(12, 0, -3)]
        mock_get_prev_quarter.side_effect = (
            prev_quarters_2022 + prev_quarters_2021 + prev_quarters_2020 + prev_quarters_2019
        )
        mock_income_statement_class.return_value = mock_income_statement

        result = margins_plot()
        data = json.loads(result)

        assert isinstance(data, list)
        assert len(data) == 3
        assert data[0]["name"] == "Gross Margin"
        assert data[1]["name"] == "Operating Margin"
        assert data[2]["name"] == "Net Margin"
        assert len(data[0]["x"]) == 16
        assert len(data[0]["y"]) == 16


def test_asset_distribution_plot():
    with (
        patch("fin_api.lib.plots.get_accounts") as mock_get_accounts,
        patch("fin_api.lib.plots.cached_get_account_balance") as mock_get_balance,
    ):
        mock_get_accounts.return_value = [MagicMock()]
        mock_get_balance.return_value = 100

        result = asset_distribution_plot()
        data = json.loads(result)

        assert isinstance(data, list)
        assert len(data) == 5
        for item in data:
            assert "value" in item
            assert "label" in item
            assert "color" in item


def test_operating_expenses_plot(mock_income_statement):
    with (
        patch("fin_api.lib.plots.get_last_of_months") as mock_get_months,
        patch("fin_api.lib.plots.IncomeStatement") as mock_income_statement_class,
    ):
        mock_get_months.return_value = [datetime(2023, i, 1) for i in range(1, 13)]
        mock_income_statement_class.return_value = mock_income_statement

        result = operating_expenses_plot()
        data = json.loads(result)

        # Test only verifies it runs. Mock data to do more
        assert isinstance(data, list)
        assert len(data) == 2
        assert data[0]["name"] == "Revenue"
        assert data[1]["name"] == "Op Expenses"


def test_histograms_plot(mock_income_statement):
    with (
        patch("fin_api.lib.plots.get_quarter") as mock_get_quarter,
        patch("fin_api.lib.plots.get_prev_quarter") as mock_get_prev_quarter,
        patch("fin_api.lib.plots.IncomeStatement") as mock_income_statement_class,
    ):
        mock_get_quarter.return_value = [datetime(2023, 3, 31)]
        prev_quarters_2022 = [datetime(2022, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2021 = [datetime(2021, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2020 = [datetime(2020, i, 1) for i in range(12, 0, -3)]
        prev_quarters_2019 = [datetime(2019, i, 1) for i in range(12, 0, -3)]
        mock_get_prev_quarter.side_effect = (
            prev_quarters_2022 + prev_quarters_2021 + prev_quarters_2020 + prev_quarters_2019
        )
        mock_income_statement_class.return_value = mock_income_statement

        result = histograms_plot()
        data = json.loads(result)

        assert isinstance(data, dict)
        assert "data" in data
        assert len(data["data"]) == 4
        assert data["data"][0]["type"] == "histogram"
        assert data["data"][0]["name"] == "Revenues"


def test_get_first_of_months():
    result = get_first_of_months(3)
    assert len(result) == 4
    assert all(isinstance(date, datetime) for date in result)
    assert all(date.day == 1 for date in result)


def test_get_last_of_months():
    result = get_last_of_months(3)
    assert len(result) == 4
    assert all(isinstance(date, datetime) for date in result)
    assert all(date.hour == 23 and date.minute == 59 and date.second == 59 for date in result)
