import json
from datetime import datetime, timedelta

from flask import url_for

from fin_api.models import JournalEntry


def test_list_journal_entries(client, sample_journal_entry):
    response = client.get(url_for("journal_entry_list"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert len(data) >= 1
    assert data[0]["id"] == sample_journal_entry.id


def test_list_journal_entries_with_date_filter(client, sample_journal_entry):
    from_date = (datetime.now() - timedelta(days=1)).strftime("%Y-%m-%d")
    to_date = (datetime.now() + timedelta(days=1)).strftime("%Y-%m-%d")
    response = client.get(url_for("journal_entry_list", from_date=from_date, to_date=to_date))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert len(data) >= 1
    assert data[0]["id"] == sample_journal_entry.id


def test_list_journal_entries_with_account_filter(client, sample_journal_entry, sample_dr_account):
    response = client.get(url_for("journal_entry_list", account_id=sample_dr_account.id))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert len(data) >= 1
    assert data[0]["id"] == sample_journal_entry.id


def test_create_journal_entry(client, sample_dr_account, sample_cr_account):
    new_entry = {
        "timestamp": datetime.now().strftime("%Y-%m-%d %H:%M"),
        "description": "Test journal entry",
        "debited_account_id": sample_dr_account.id,
        "debited_amount": 100.0,
        "credited_account_id": sample_cr_account.id,
        "credited_amount": 100.0,
    }
    response = client.post(url_for("journal_entry_list"), json=new_entry)
    assert response.status_code == 201
    data = json.loads(response.data)
    assert data["description"] == "Test journal entry"


def test_get_journal_entry(client, sample_journal_entry):
    response = client.get(url_for("journal_entry_item", id=sample_journal_entry.id))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data["id"] == sample_journal_entry.id


def test_update_journal_entry(client, sample_journal_entry):
    updated_entry = {
        "timestamp": sample_journal_entry.timestamp.strftime("%Y-%m-%d %H:%M"),
        "description": "Updated test journal entry",
        "debited_account_id": sample_journal_entry.debited_account_id,
        "debited_amount": sample_journal_entry.debited_amount,
        "credited_account_id": sample_journal_entry.credited_account_id,
        "credited_amount": sample_journal_entry.credited_amount,
    }
    response = client.put(url_for("journal_entry_item", id=sample_journal_entry.id), json=updated_entry)
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data["description"] == "Updated test journal entry"


def test_delete_journal_entry(client, session, sample_journal_entry):
    response = client.delete(url_for("journal_entry_item", id=sample_journal_entry.id))
    assert response.status_code == 204
    # Verify the entry is deleted
    assert session.query(JournalEntry).get(sample_journal_entry.id) is None


def test_get_nonexistent_journal_entry(client):
    response = client.get(url_for("journal_entry_item", id=99999))
    assert response.status_code == 404


def test_update_nonexistent_journal_entry(client, sample_journal_entry):
    updated_entry = {
        "timestamp": sample_journal_entry.timestamp.strftime("%Y-%m-%d %H:%M"),
        "description": "Updated test journal entry",
        "debited_account_id": sample_journal_entry.debited_account_id,
        "debited_amount": sample_journal_entry.debited_amount,
        "credited_account_id": sample_journal_entry.credited_account_id,
        "credited_amount": sample_journal_entry.credited_amount,
    }
    response = client.put(url_for("journal_entry_item", id=99999), json=updated_entry)
    assert response.status_code == 404


def test_delete_nonexistent_journal_entry(client):
    response = client.delete(url_for("journal_entry_item", id=99999))
    assert response.status_code == 404


def test_create_journal_entry_invalid_data(client):
    invalid_entry = {}  # Empty data
    response = client.post(url_for("journal_entry_list"), json=invalid_entry)
    assert response.status_code == 400


def test_update_journal_entry_invalid_data(client, sample_journal_entry):
    invalid_update = {"timestamp": "invalid_date"}
    response = client.put(url_for("journal_entry_item", id=sample_journal_entry.id), json=invalid_update)
    assert response.status_code == 400


def test_list_journal_entries_invalid_date_format(client):
    response = client.get(url_for("journal_entry_list", from_date="invalid_date"))
    assert response.status_code == 400
