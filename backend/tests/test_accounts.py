import json
from datetime import datetime

import pytest
from flask import url_for

from fin_api.lib.tools import get_accounts
from fin_api.models import Account


@pytest.fixture
def sample_accounts(session):
    accounts = [
        Account(
            name="cash",
            human_name="Cash",
            account_type="cash_asset",
            balance_type="debit",
            is_archived=False,
            financial_statement="balance_sheet",
        ),
        Account(
            name="revenue",
            human_name="Revenue",
            account_type="revenue",
            balance_type="credit",
            is_archived=False,
            financial_statement="balance_sheet",
        ),
        Account(
            name="expense",
            human_name="Expense",
            account_type="expense",
            balance_type="debit",
            is_archived=False,
            financial_statement="income_statement",
        ),
        Account(
            name="old_account",
            human_name="Old Account",
            account_type="noncash_liquid_asset",
            balance_type="debit",
            is_archived=True,
            financial_statement="balance_sheet",
        ),
    ]
    session.add_all(accounts)
    session.commit()
    return accounts


def test_get_accounts(session, sample_accounts):
    # Test getting all non-archived accounts
    all_accounts = get_accounts()
    assert len(all_accounts) == 3
    assert all(not account.is_archived for account in all_accounts)

    # Test getting specific account types
    asset_accounts = get_accounts(account_types=["cash_asset"])
    assert len(asset_accounts) == 1
    assert asset_accounts[0].name == "cash"

    # Test getting multiple account types
    asset_and_revenue_accounts = get_accounts(account_types=["cash_asset", "revenue"])
    assert len(asset_and_revenue_accounts) == 2
    assert set(account.name for account in asset_and_revenue_accounts) == {"cash", "revenue"}

    # Test including archived accounts
    all_accounts_with_archived = get_accounts(include_archived=True)
    assert len(all_accounts_with_archived) == 4
    assert any(account.is_archived for account in all_accounts_with_archived)

    # Test ordering
    ordered_accounts = get_accounts(include_archived=True)
    assert {account.name for account in ordered_accounts} == {"cash", "revenue", "expense", "old_account"}

    # Test with non-existent account type
    non_existent_accounts = get_accounts(account_types=["non_existent"])
    assert len(non_existent_accounts) == 0


def test_get_account_balances(client, sample_journal_entry, sample_dr_account):
    # Test getting all balances (original behavior)
    response = client.get(url_for("account_balances"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)
    assert len(data) > 0
    assert "account" in data[0]
    assert "balance" in data[0]

    # Test filtering by account_id
    response = client.get(url_for("account_balances", account_id=sample_dr_account.id))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert len(data) == 1
    assert data[0]["account"]["id"] == sample_dr_account.id

    # Test filtering by date
    response = client.get(url_for("account_balances", up_to_date=datetime.now().strftime("%Y-%m-%d")))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)
    assert len(data) > 0

    # Test filtering by both account_id and date
    response = client.get(
        url_for("account_balances", account_id=sample_dr_account.id, up_to_date=datetime.now().strftime("%Y-%m-%d"))
    )
    assert response.status_code == 200
    data = json.loads(response.data)
    assert len(data) == 1
    assert data[0]["account"]["id"] == sample_dr_account.id

    # Test with invalid date format
    response = client.get(url_for("account_balances", up_to_date="invalid-date"))
    assert response.status_code == 400

    # Test with non-existent account_id
    response = client.get(url_for("account_balances", account_id=99999))
    assert response.status_code == 404


def test_list_accounts(client, sample_accounts):
    response = client.get(url_for("account_list"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert len(data) == len(sample_accounts) - 1  # Exclude archived account


def test_list_accounts_with_type_filter(client, sample_accounts):
    response = client.get(url_for("account_list", account_types="cash_asset"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert all(account["account_type"] == "cash_asset" for account in data)


def test_create_account(client):
    new_account = {
        "human_name": "Test Account",
        "description": "A test account",
        "balance_type": "debit",
        "account_type": "cash_asset",
        "financial_statement": "balance_sheet",
    }
    response = client.post(url_for("account_list"), json=new_account)
    assert response.status_code == 201
    data = json.loads(response.data)
    assert data["human_name"] == "Test Account"


def test_get_account(client, sample_accounts):
    account_id = sample_accounts[0].id
    response = client.get(url_for("account_item", id=account_id))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data["id"] == account_id


def test_update_account(client, sample_accounts):
    account_id = sample_accounts[0].id
    updated_data = {
        "human_name": "Updated Account",
        "description": "An updated account",
        "balance_type": "credit",
        "account_type": "revenue",
        "financial_statement": "income_statement",
        "is_archived": True,
    }
    response = client.put(url_for("account_item", id=account_id), json=updated_data)
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data["human_name"] == "Updated Account"
    assert data["is_archived"] is True


def test_get_nonexistent_account(client):
    response = client.get(url_for("account_item", id=99999))
    assert response.status_code == 404


def test_update_nonexistent_account(client):
    response = client.put(url_for("account_item", id=99999), json={"human_name": "Nonexistent"})
    assert response.status_code == 404
