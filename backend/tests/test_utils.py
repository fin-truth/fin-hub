from datetime import date, datetime
from unittest.mock import patch

from fin_api.lib.tools import (
    first_of_month,
    get_account_balance,
    get_next_quarter,
    get_prev_quarter,
    get_quarter,
)
from fin_api.models.journal_entry import JournalEntry


class MockDate(date):
    @classmethod
    def today(cls):
        return date(2024, 3, 15)


def test_first_of_month():
    # Test with a date in the middle of the month
    with patch("fin_api.lib.tools.date", MockDate):
        result = first_of_month()
        assert result == date(2024, 3, 1)


def test_get_quarter():
    # Test with a date in the middle of the first quarter
    with patch("fin_api.lib.tools.date", MockDate):
        start, end = get_quarter()
        assert start == datetime(2024, 1, 1, 0, 0, 0)
        assert end == datetime(2024, 3, 31, 23, 59, 59, 999000)


def test_get_next_quarter():
    # Test transition from Q4 to Q1 of next year
    date = datetime(2024, 12, 1)
    result = get_next_quarter(date)
    assert result == datetime(2025, 1, 1, 0, 0, 0)

    # Test transition from Q2 to Q3
    date = datetime(2024, 6, 30)
    result = get_next_quarter(date)
    assert result == datetime(2024, 7, 1, 0, 0, 0)


def test_get_prev_quarter():
    # Test transition from Q1 to Q4 of previous year
    date = datetime(2024, 1, 1)
    result = get_prev_quarter(date)
    assert result == datetime(2023, 10, 1, 0, 0, 0)

    # Test transition from Q3 to Q2
    date = datetime(2024, 7, 1)
    result = get_prev_quarter(date)
    assert result == datetime(2024, 4, 1, 0, 0, 0)


def test_account_balance(session, sample_cr_account, sample_dr_account, sample_journal_entry):
    balance = get_account_balance(sample_cr_account)
    assert balance == 100.0
    balance = get_account_balance(sample_dr_account)
    assert balance == 100.0
    new_entry = JournalEntry(
        timestamp=datetime.now(),
        description="Test journal entry 2",
        debited_account_id=sample_cr_account.id,
        debited_amount=30.0,
        credited_account_id=sample_dr_account.id,
        credited_amount=30.0,
    )
    session.add(new_entry)
    session.commit()
    balance = get_account_balance(sample_cr_account, up_to=datetime.now())  # Defeat the cache
    assert balance == 70.0
    balance = get_account_balance(sample_dr_account, up_to=datetime.now())  # Defeat the cache
    assert balance == 70.0
