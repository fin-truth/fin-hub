from numpy import average

from fin_api.lib.plots import get_first_of_months
from fin_api.lib.statements.income_statement import IncomeStatement


def test_operating_expenses(app):
    with app.test_client():
        yoy_first_of_months = get_first_of_months(n=12)
        income_statements = [IncomeStatement(up_to) for up_to in yoy_first_of_months]
        op_expenses = [sum(statement.operating_expenses.values()) for statement in income_statements]
        op_expenses = [exp for exp in op_expenses if exp > 0.01]
        print(average(op_expenses))
    pass
