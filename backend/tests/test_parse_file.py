import io
from datetime import datetime
from unittest.mock import MagicMock, patch

import pytest
from sqlalchemy.orm import joinedload

from fin_api.lib.parse_file import (
    ChaseParser,
    ImportParser,
    MarcusParser,
    MTBankParser,
    SchwabParser,
    USAAParser,
    VenmoParser,
    ZenefitsParser,
)
from fin_api.models import Account, JournalEntry


@pytest.fixture
def mock_marcus_pdf():
    mock_pdf = MagicMock()
    mock_pdf.elements.filter_by_text_contains.return_value = [
        MagicMock(text=lambda: "Statement Period\n01/01/2023 to 01/31/2023")
    ]
    mock_pdf.elements.to_the_right_of.return_value = [MagicMock(text=lambda: "$10.50")]
    return mock_pdf


@pytest.fixture
def mock_usaa_csv_content():
    return (
        "Date,Original Description,Amount\n" "2023-01-15,Grocery Store,-50.00\n" "2023-01-20,Salary Deposit,1000.00\n"
    )


@pytest.fixture
def mock_venmo_csv_content():
    return (
        "Account Statement - (@John-Doe) - September 30th to November 1st 2024 ,,,,,,,,,,,,,,,,,,,,,,\n"
        "Account Activity,,,,,,,,,,,,,,,,,,,,,\n"
        ",ID,Datetime,Type,Status,Note,From,To,Amount (total),Amount (tip),Amount (tax),Amount (fee),Tax Rate,Tax Exempt,Funding Source,Destination,Beginning Balance,Ending Balance,Statement Period Venmo Fees,Terminal Location,Year to Date Venmo Fees,Disclaimer\n"
        ",,,,,,,,,,,,,,,,$477.87,,,,,\n"
        ",4176193425614986331,2024-10-11T01:10:51,Payment,Complete,Dinner settle up,John Doe,Jane Smith,- $306.73,,0,,0,,Venmo balance,,,,,Venmo,,\n"
        ",4122018581209417952,2024-10-19T02:04:23,Payment,Complete,Rent paid,Al Gore,John Doe,+ $11.00,,0,,0,,,Venmo balance,,,,Venmo,,\n"
    )


@pytest.fixture
def mock_chase_csv_content():
    return (
        "Transaction Date,Post Date,Description,Category,Type,Amount,Memo\n"
        "01/15/2023,01/16/2023,GROCERY STORE,Groceries,Sale,-50.00,\n"
        "01/20/2023,01/21/2023,SALARY DEPOSIT,Income,Payment,1000.00,\n"
    )


@pytest.fixture
def mock_mtbank_csv_content():
    return "Date,Principal,Interest,Escrow,Fees\n" "01/15/2023,1000.00,500.00,200.00,50.00\n"


@pytest.fixture
def mock_schwab_pdf():
    mock_pdf = MagicMock()

    # Mock for statement period
    statement_period_mock = MagicMock()
    statement_period_mock.text.return_value = "Statement Period"

    # Mock for dividends and interest
    divs_interest_mock = MagicMock()
    divs_interest_mock.extract_single_element.return_value = MagicMock()

    # Mock for unrealized change
    unrealized_mock = MagicMock()
    unrealized_mock.extract_single_element.return_value = MagicMock()

    # Set up side_effect for filter_by_text_contains
    mock_pdf.elements.filter_by_text_contains.side_effect = [
        [statement_period_mock],
        divs_interest_mock,
        unrealized_mock,
    ]

    # Mock for vertically_in_line_with
    mock_pdf.elements.vertically_in_line_with.return_value = [
        MagicMock(),
        MagicMock(text=lambda: "January 1 - 31, 2023"),
    ]

    # Mock for to_the_right_of
    mock_pdf.elements.to_the_right_of.side_effect = [
        [MagicMock(text=lambda: "100.50")],
        [MagicMock(text=lambda: "500.75")],
    ]

    return mock_pdf


@pytest.fixture
def mock_zenefits_pdf():
    mock_pdf = MagicMock()

    # Mock for pay day and net pay
    mock_pdf.elements.filter_by_text_contains.return_value = [MagicMock()]
    mock_pdf.elements.to_the_right_of.return_value = [
        MagicMock(text=lambda: "Pay Day:\nPay Period:\nJun 15, 2023\n$5000.00")
    ]

    # Mock for 401k contribution
    mock_pdf.elements.filter_by_text_contains.side_effect = [
        [MagicMock()],  # Pay Day
        [MagicMock()],  # 401k
        [MagicMock(), MagicMock()],  # Health Savings Account (two occurrences)
        [MagicMock(), MagicMock()],  # Dental (two occurrences)
        [MagicMock(), MagicMock()],  # Vision (two occurrences)
        [MagicMock()],  # Taxes
    ]
    mock_pdf.elements.to_the_right_of.side_effect = [
        [MagicMock(text=lambda: "Pay Day:\nPay Period:\nJun 15, 2023\n$5000.00")],  # Pay Day
        [MagicMock(text=lambda: "$200.00")],  # 401k
        [MagicMock(text=lambda: "$50.00")],  # HSA
        [MagicMock(text=lambda: "$100.00")],  # HSA 2
        [MagicMock(text=lambda: "$1000.00")],  # Dental
        [MagicMock(text=lambda: "$10000.00")],  # Vision
        [MagicMock(text=lambda: "$100000.00")],  #
        [MagicMock(text=lambda: "$100000.00")],  #
    ]

    # Mock for taxes
    mock_pdf.elements.vertically_in_line_with.return_value = [
        MagicMock(text=lambda: "$1000.00 Total"),
        MagicMock(text=lambda: "$1000.00 Total"),
        MagicMock(text=lambda: "$1000.00 Total"),
    ]

    return mock_pdf


def test_marcus_parser_initialization():
    # Arrange
    account_id = 1
    revenue_account_id = 2

    # Act
    parser = MarcusParser(account_id, revenue_account_id)

    # Assert
    assert isinstance(parser, MarcusParser)
    assert isinstance(parser, ImportParser)
    assert parser.account_id == account_id
    assert parser.revenue_account_id == revenue_account_id


def test_marcus_parser_parse_file(mock_marcus_pdf, app, session, sample_dr_account):
    # Arrange
    account_id = 1
    revenue_account_id = 2
    parser = MarcusParser(account_id, revenue_account_id)

    with patch("fin_api.lib.parse_file.load", return_value=mock_marcus_pdf):
        # Act
        with app.app_context():
            parser.parse_file(io.BytesIO(b"mock pdf content"))

        # Assert
        journal_entry = session.query(JournalEntry).first()
        assert journal_entry is not None
        assert datetime.date(journal_entry.timestamp) == datetime.date(datetime(2023, 1, 31))
        assert journal_entry.debited_amount == 10.50
        assert journal_entry.credited_amount == 10.50
        assert journal_entry.debited_account_id == account_id
        assert journal_entry.credited_account_id == revenue_account_id
        assert "interest and dividends for January" in journal_entry.description


def test_usaa_parser_parse_file(mock_usaa_csv_content, app, session, sample_dr_account):
    # Arrange
    account_id = 1
    revenue_account_id = 2
    parser = USAAParser(account_id, revenue_account_id)

    # Act
    with app.app_context():
        parser.parse_file(io.BytesIO(mock_usaa_csv_content.encode("utf-8")))

    # Assert
    journal_entries = session.query(JournalEntry).all()
    assert len(journal_entries) == 2

    # Check first entry (expense)
    expense_entry = journal_entries[0]
    assert datetime.date(expense_entry.timestamp) == datetime.date(datetime(2023, 1, 15))
    assert expense_entry.debited_amount == 50.00
    assert expense_entry.credited_amount == 50.00
    assert expense_entry.debited_account_id == app.config["NOT_CLASSIFIED_ACCOUNT_ID"]
    assert expense_entry.credited_account_id == account_id
    assert expense_entry.description == "Grocery Store"

    # Check second entry (income)
    income_entry = journal_entries[1]
    assert datetime.date(income_entry.timestamp) == datetime.date(datetime(2023, 1, 20))
    assert income_entry.debited_amount == 1000.00
    assert income_entry.credited_amount == 1000.00
    assert income_entry.debited_account_id == account_id
    assert income_entry.credited_account_id == revenue_account_id
    assert income_entry.description == "Salary Deposit"


def test_venmo_parser_parse_file(
    mock_venmo_csv_content, app, session, sample_unclassified_expense_account, base_accounts, sample_dr_account
):
    # Arrange
    account_id = sample_dr_account.id
    revenue_account_id = base_accounts["revenue"].id
    unclassified_expense_account_id = sample_unclassified_expense_account.id
    parser = VenmoParser(account_id, revenue_account_id)

    # Act
    with app.app_context():
        parser.parse_file(io.BytesIO(mock_venmo_csv_content.encode("utf-8")))

    # Assert
    journal_entries = session.query(JournalEntry).all()
    assert len(journal_entries) == 2

    # Check first entry (income)
    expense_entry = journal_entries[0]
    assert expense_entry.timestamp.date() == datetime(2024, 10, 11).date()
    assert expense_entry.debited_amount == 306.73
    assert expense_entry.credited_amount == 306.73
    assert expense_entry.debited_account_id == unclassified_expense_account_id
    assert expense_entry.credited_account_id == account_id
    assert expense_entry.description == "Dinner settle up From Jane Smith to John Doe"

    # Check second entry (expense)
    income_entry = journal_entries[1]
    assert income_entry.timestamp.date() == datetime(2024, 10, 19).date()
    assert income_entry.debited_amount == 11.00
    assert income_entry.credited_amount == 11.00
    assert income_entry.debited_account_id == account_id
    assert income_entry.credited_account_id == revenue_account_id
    assert income_entry.description == "Rent paid From John Doe to Al Gore"


def test_chase_parser_parse_file(
    mock_chase_csv_content, app, session, sample_unclassified_expense_account, base_accounts, sample_dr_account
):
    # Arrange
    account_id = sample_dr_account.id
    revenue_account_id = base_accounts["revenue"].id
    unclassified_expense_account_id = sample_unclassified_expense_account.id
    parser = ChaseParser(account_id, revenue_account_id)

    # Act
    with app.app_context():
        parser.parse_file(io.BytesIO(mock_chase_csv_content.encode("utf-8")))

    # Assert
    journal_entries = session.query(JournalEntry).all()
    assert len(journal_entries) == 2

    # Check first entry (expense)
    expense_entry = journal_entries[0]
    assert expense_entry.timestamp.date() == datetime(2023, 1, 16).date()
    assert expense_entry.debited_amount == 50.00
    assert expense_entry.credited_amount == 50.00
    assert expense_entry.debited_account_id == unclassified_expense_account_id
    assert expense_entry.credited_account_id == account_id
    assert expense_entry.description == "GROCERY STORE"

    # Check second entry (income)
    income_entry = journal_entries[1]
    assert income_entry.timestamp.date() == datetime(2023, 1, 21).date()
    assert income_entry.debited_amount == 1000.00
    assert income_entry.credited_amount == 1000.00
    assert income_entry.debited_account_id == account_id
    assert income_entry.credited_account_id == revenue_account_id
    assert income_entry.description == "SALARY DEPOSIT"


def test_mtbank_parser_parse_file(
    mock_mtbank_csv_content, app, session, sample_dr_account, base_accounts, sample_real_estate_accounts
):
    # Arrange
    account_id = sample_dr_account.id
    revenue_account_id = base_accounts["revenue"].id
    sample_expense_account_id = sample_real_estate_accounts[0].id
    sample_note_account_id = sample_real_estate_accounts[1].id
    parser = MTBankParser(account_id, revenue_account_id)

    # Act
    with app.app_context():
        parser.parse_file(io.BytesIO(mock_mtbank_csv_content.encode("utf-8")))

    # Assert
    journal_entries = session.query(JournalEntry).all()
    assert len(journal_entries) == 2  # One for principal, one for interest+escrow+fees

    # Check principal entry
    principal_entry = journal_entries[0]
    assert principal_entry.timestamp.date() == datetime(2023, 1, 15).date()
    assert principal_entry.debited_amount == 1000.00
    assert principal_entry.credited_amount == 1000.00
    assert principal_entry.debited_account_id == sample_note_account_id
    assert principal_entry.credited_account_id == account_id
    assert "principal payment" in principal_entry.description

    # Check interest+escrow+fees entry
    iti_entry = journal_entries[1]
    assert iti_entry.timestamp.date() == datetime(2023, 1, 15).date()
    assert iti_entry.debited_amount == 750.00  # 500 + 200 + 50
    assert iti_entry.credited_amount == 750.00
    assert iti_entry.debited_account_id == sample_expense_account_id
    assert iti_entry.credited_account_id == account_id
    assert "ITI payment" in iti_entry.description


def test_schwab_parser_parse_file(mock_schwab_pdf, app, session, sample_dr_account, base_accounts):
    # Arrange
    account_id = sample_dr_account.id
    revenue_account_id = base_accounts["revenue"].id
    parser = SchwabParser(account_id, revenue_account_id)

    # Mock the Account queries
    mock_account = MagicMock()
    mock_account.human_name = "Schwab Account"
    mock_unrealized_change = MagicMock()
    mock_unrealized_change.id = 999

    with patch("fin_api.lib.parse_file.db.session.query") as mock_query:
        mock_query().get.return_value = mock_account
        mock_query().filter.return_value.one.return_value = mock_unrealized_change

        # Act
        with patch("fin_api.lib.parse_file.load", return_value=mock_schwab_pdf):
            with app.app_context():
                parser.parse_file(io.BytesIO(b"mock pdf content"))

    # Assert
    journal_entries = session.query(JournalEntry).all()
    assert len(journal_entries) == 2  # One for dividends/interest, one for unrealized change

    # Check dividends and interest entry
    div_entry = journal_entries[0]
    assert div_entry.timestamp.date() == datetime(2023, 1, 31).date()
    assert div_entry.debited_amount == 100.50
    assert div_entry.credited_amount == 100.50
    assert div_entry.debited_account_id == account_id
    assert div_entry.credited_account_id == revenue_account_id
    assert "Schwab Account interest and dividends for January" in div_entry.description

    # Check unrealized change entry
    unrealized_entry = journal_entries[1]
    assert unrealized_entry.timestamp.date() == datetime(2023, 1, 31).date()
    assert unrealized_entry.debited_amount == 500.75
    assert unrealized_entry.credited_amount == 500.75
    assert unrealized_entry.debited_account_id == account_id
    assert unrealized_entry.credited_account_id == 999  # mock_unrealized_change.id
    assert "Schwab Account unrealized change for January" in unrealized_entry.description


def test_zenefits_parser_parse_file(mock_zenefits_pdf, app, session, sample_dr_account, base_accounts):
    # Arrange
    account_id = sample_dr_account.id
    revenue_account_id = base_accounts["revenue"].id
    tax_expense_account_id = base_accounts["tax_expense"].id
    parser = ZenefitsParser(account_id, revenue_account_id)

    # Mock necessary accounts
    hsa_account = Account(
        name="health_savings_account",
        human_name="HSA Account",
        description="",
        balance_type="debit",
        account_type="noncash_liquid_asset",
        financial_statement="balance_sheet",
    )
    emp_401k_account = Account(
        name="guideline_401k",
        human_name="401k Account",
        description="",
        balance_type="debit",
        account_type="noncash_illiquid_asset",
        financial_statement="balance_sheet",
    )
    healthcare_expense_account = Account(
        name="healthcare",
        human_name="Healthcare Expense",
        description="",
        balance_type="debit",
        account_type="expense",
        financial_statement="income_statement",
    )

    session.add_all([hsa_account, emp_401k_account, healthcare_expense_account])
    session.commit()
    hsa_account_id = hsa_account.id
    emp_401k_account_id = emp_401k_account.id
    healthcare_expense_account_id = healthcare_expense_account.id

    with patch("fin_api.lib.parse_file.load", return_value=mock_zenefits_pdf):
        # Act
        with app.app_context():
            parser.parse_file(io.BytesIO(b"mock pdf content"))

    # Refresh the session to ensure all objects are attached
    session.expire_all()

    # Query for journal entries with eager loading
    journal_entries = session.query(JournalEntry).all()

    assert len(journal_entries) == 6  # Net pay, HSA, second hsa, 401k, Healthcare, Taxes

    # Helper function to find entry by description
    def find_entry(description):
        return next(je for je in journal_entries if description in je.description)

    # Check net pay entry
    net_pay_entry = find_entry("Net Pay")
    assert net_pay_entry.timestamp.date() == datetime(2023, 6, 15).date()
    assert net_pay_entry.debited_amount == 5000.00
    assert net_pay_entry.credited_amount == 5000.00
    assert net_pay_entry.debited_account_id == account_id
    assert net_pay_entry.credited_account_id == revenue_account_id

    # Check HSA entry
    hsa_entry = find_entry("HSA Account")
    assert hsa_entry.timestamp.date() == datetime(2023, 6, 15).date()
    assert hsa_entry.debited_amount == 50.00
    assert hsa_entry.credited_amount == 50.00
    assert hsa_entry.debited_account_id == hsa_account_id
    assert hsa_entry.credited_account_id == revenue_account_id

    # Check 401k entry
    k401_entry = find_entry("401k Account")
    assert k401_entry.timestamp.date() == datetime(2023, 6, 15).date()
    assert k401_entry.debited_amount == 200.00
    assert k401_entry.credited_amount == 200.00
    assert k401_entry.debited_account_id == emp_401k_account_id
    assert k401_entry.credited_account_id == revenue_account_id

    # Check healthcare entry
    healthcare_entry = find_entry("Employer covered healthcare costs")
    assert healthcare_entry.timestamp.date() == datetime(2023, 6, 15).date()
    assert healthcare_entry.debited_amount == 11000.00
    assert healthcare_entry.credited_amount == 11000.00
    assert healthcare_entry.debited_account_id == healthcare_expense_account_id
    assert healthcare_entry.credited_account_id == revenue_account_id

    # Check tax entry
    tax_entry = find_entry("Federal and state income taxes")
    assert tax_entry.timestamp.date() == datetime(2023, 6, 15).date()
    assert tax_entry.debited_amount == 1000.00
    assert tax_entry.credited_amount == 1000.00
    assert tax_entry.debited_account_id == tax_expense_account_id
    assert tax_entry.credited_account_id == revenue_account_id
