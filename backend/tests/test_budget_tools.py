from datetime import datetime, timedelta

import pytest

from fin_api.lib.budget_tools import (
    get_current_allotment,
    get_last_period_total,
    get_soft_total,
)
from fin_api.models import BudgetAllotment, JournalEntry


@pytest.fixture
def sample_budget_allotments(session, sample_unclassified_expense_account):
    allotments = [
        BudgetAllotment(
            timestamp=datetime.now() - timedelta(days=30),
            expense_account_id=sample_unclassified_expense_account.id,
            allotment=500.0,
        ),
        BudgetAllotment(
            timestamp=datetime.now() - timedelta(days=15),
            expense_account_id=sample_unclassified_expense_account.id,
            allotment=600.0,
        ),
        BudgetAllotment(
            timestamp=datetime.now(), expense_account_id=sample_unclassified_expense_account.id, allotment=700.0
        ),
    ]
    session.add_all(allotments)
    session.commit()
    return allotments


@pytest.fixture
def sample_journal_entries(session, sample_unclassified_expense_account, sample_dr_account):
    now = datetime.now()
    entries = [
        JournalEntry(
            timestamp=now - timedelta(days=45),
            description="Old entry",
            debited_account_id=sample_unclassified_expense_account.id,
            debited_amount=100.0,
            credited_account_id=sample_dr_account.id,
            credited_amount=100.0,
        ),
        JournalEntry(
            timestamp=now - timedelta(days=15),
            description="Recent entry",
            debited_account_id=sample_unclassified_expense_account.id,
            debited_amount=200.0,
            credited_account_id=sample_dr_account.id,
            credited_amount=200.0,
        ),
        JournalEntry(
            timestamp=now,
            description="Current entry",
            debited_account_id=sample_unclassified_expense_account.id,
            debited_amount=300.0,
            credited_account_id=sample_dr_account.id,
            credited_amount=300.0,
        ),
    ]
    session.add_all(entries)
    session.commit()
    return entries


def test_get_current_allotment(session, sample_unclassified_expense_account, sample_budget_allotments):
    current_allotment = get_current_allotment(sample_unclassified_expense_account)
    assert current_allotment is not None
    assert current_allotment.allotment == 700.0


def test_get_soft_total(session, sample_unclassified_expense_account, sample_journal_entries):
    now = datetime.now()
    from_date = now - timedelta(days=30)
    to_date = now

    soft_total = get_soft_total(sample_unclassified_expense_account, from_date, to_date)
    assert soft_total == 500.0  # Sum of the two most recent entries


def test_get_last_period_total(session, sample_unclassified_expense_account, sample_journal_entries):
    now = datetime.now()
    from_date = now - timedelta(days=30)
    to_date = now

    last_period_total = get_last_period_total(sample_unclassified_expense_account, from_date, to_date)
    assert last_period_total == 100.0  # The oldest entry
