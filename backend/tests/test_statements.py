import json
from datetime import datetime

from flask import url_for


def test_get_balance_sheet(client):
    response = client.get(url_for("statements_balance_sheet"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert "assets" in data
    assert "liabilities" in data
    assert "equity" in data
    assert "total_liabilities_and_equity" in data
    assert "as_of_date" in data


def test_get_balance_sheet_with_date(client):
    year = datetime.now().year
    quarter = (datetime.now().month - 1) // 3 + 1
    response = client.get(url_for("statements_balance_sheet", year=year, quarter=quarter))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert "as_of_date" in data


def test_get_income_statement(client):
    response = client.get(url_for("statements_income_statement"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert "up_to" in data
    assert "revenues" in data
    assert "operating_expenses" in data
    assert "operating_profit" in data
    assert "net_income" in data


def test_get_income_statement_with_date(client):
    year = datetime.now().year
    quarter = (datetime.now().month - 1) // 3 + 1
    response = client.get(url_for("statements_income_statement", year=year, quarter=quarter))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert "up_to" in data


def test_get_cash_flow_statement(client, base_accounts):
    response = client.get(url_for("statements_cash_flow"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert "operating_flows" in data
    assert "investment_flows" in data
    assert "financing_flows" in data
    assert "total_cash_flow" in data


def test_get_cash_flow_statement_with_date(client, base_accounts):
    year = datetime.now().year
    quarter = (datetime.now().month - 1) // 3 + 1
    response = client.get(url_for("statements_cash_flow", year=year, quarter=quarter))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert "operating_flows" in data


def test_close_statement(client, base_accounts):
    close_date = datetime.now().strftime("%Y-%m-%d")
    response = client.post(url_for("statements_close"), json={"date": close_date})
    assert response.status_code == 200
    data = json.loads(response.data)
    assert "message" in data
    assert data["message"] == "Close performed successfully"


def test_close_statement_invalid_date(client):
    response = client.post(url_for("statements_close"), json={"date": "invalid_date"})
    assert response.status_code == 500
    data = json.loads(response.data)
    assert "error" in data


def test_close_statement_missing_date(client):
    response = client.post(url_for("statements_close"), json={})
    assert response.status_code == 400
    data = json.loads(response.data)
    assert "error" in data
