import io
import json
from unittest.mock import patch

import pytest
from flask import url_for

from fin_api.lib.parse_file import SUPPORTED_INPUT_PROVIDERS


@pytest.fixture
def mock_file():
    return io.BytesIO(b"mock file content")


def test_import_file_success(client, mock_file, sample_dr_account, base_accounts):
    with patch("fin_api.api.import_files.get_parsing_handler") as mock_get_parsing_handler:
        mock_parser = mock_get_parsing_handler.return_value
        mock_parser.parse_file.return_value = None  # Simulate successful parsing

        data = {
            "provider": "USAA",
            "account_id": str(sample_dr_account.id),
            "revenue_account_id": str(base_accounts["revenue"].id),
            "file": (mock_file, "test.csv"),
        }
        response = client.post(url_for("import_file"), data=data, content_type="multipart/form-data")
        assert response.status_code == 200
        result = json.loads(response.data)
        assert result["message"] == "File imported successfully"


def test_import_file_no_file(client, sample_dr_account, base_accounts):
    data = {
        "provider": "USAA",
        "account_id": str(sample_dr_account.id),
        "revenue_account_id": str(base_accounts["revenue"].id),
    }
    response = client.post(url_for("import_file"), data=data, content_type="multipart/form-data")
    assert response.status_code == 400
    result = json.loads(response.data)
    assert result["error"] == "No file part"


def test_import_file_empty_filename(client, mock_file, sample_dr_account, base_accounts):
    data = {
        "provider": "USAA",
        "account_id": str(sample_dr_account.id),
        "revenue_account_id": str(base_accounts["revenue"].id),
        "file": (mock_file, ""),
    }
    response = client.post(url_for("import_file"), data=data, content_type="multipart/form-data")
    assert response.status_code == 400
    result = json.loads(response.data)
    assert result["error"] == "No selected file"


def test_import_file_missing_fields(client, mock_file):
    data = {"file": (mock_file, "test.csv")}
    response = client.post(url_for("import_file"), data=data, content_type="multipart/form-data", buffered=True)
    assert response.status_code == 400
    result = json.loads(response.data)
    assert result["error"] == "Missing required fields"


def test_import_file_parse_error(client, mock_file, sample_dr_account, base_accounts):
    with patch("fin_api.api.import_files.get_parsing_handler") as mock_get_parsing_handler:
        mock_parser = mock_get_parsing_handler.return_value
        mock_parser.parse_file.side_effect = Exception("Parsing error")

        data = {
            "provider": "USAA",
            "account_id": str(sample_dr_account.id),
            "revenue_account_id": str(base_accounts["revenue"].id),
            "file": (mock_file, ""),
        }
        response = client.post(url_for("import_file"), data=data, content_type="multipart/form-data")
        assert response.status_code == 400
        result = json.loads(response.data)
        assert "error" in result


def test_get_supported_providers(client):
    response = client.get(url_for("supported_providers"))
    assert response.status_code == 200
    result = json.loads(response.data)
    assert isinstance(result, list)
    assert set(result) == set(SUPPORTED_INPUT_PROVIDERS)
