import json
from datetime import datetime, timedelta

from flask import url_for

from fin_api.lib.budget_tools import (
    get_current_allotment,
    get_last_period_total,
    get_soft_total,
)


def test_get_budget_data(client, sample_budget_allotments):
    response = client.get(url_for("budget_list"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)
    assert len(data) > 0
    assert "category" in data[0].keys()
    assert "allotment" in data[0].keys()


def test_get_budget_data_with_date_range(client, sample_budget_allotments):
    from_date = (datetime.now() - timedelta(days=30)).strftime("%Y-%m-%d")
    to_date = datetime.now().strftime("%Y-%m-%d")
    response = client.get(url_for("budget_list", from_date=from_date, to_date=to_date))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)
    assert len(data) > 0


def test_get_budget_data_include_misc(client, sample_budget_allotments):
    response = client.get(url_for("budget_list", include_misc="true"))
    assert response.status_code == 200
    data = json.loads(response.data)
    assert isinstance(data, list)
    assert len(data) > 0
    assert any(item["account_type"] == "misc_expense" for item in data)


def test_update_budget_allotment(client, sample_budget_data):
    response = client.put(url_for("budget_allotment"), json=sample_budget_data)
    assert response.status_code == 200
    data = json.loads(response.data)
    assert data["new_allotment"] == sample_budget_data["new_allotment"]


def test_update_budget_allotment_invalid_account(client, sample_budget_data):
    invalid_data = sample_budget_data.copy()
    invalid_data["account_id"] = 99999  # Non-existent account
    response = client.put(url_for("budget_allotment"), json=invalid_data)
    assert response.status_code == 404


def test_update_budget_allotment_missing_data(client):
    response = client.put(url_for("budget_allotment"), json={})
    assert response.status_code == 400


def test_get_current_allotment(sample_unclassified_expense_account, sample_budget_allotments):
    allotment = get_current_allotment(sample_unclassified_expense_account)
    assert allotment is not None
    assert allotment.allotment == 1000.0  # Based on the fixture data


def test_get_soft_total(sample_unclassified_expense_account, sample_unclassified_expense_journal):
    from_date = datetime.now() - timedelta(days=30)
    to_date = datetime.now()
    total = get_soft_total(sample_unclassified_expense_account, from_date, to_date)
    assert total == 100.0


def test_get_last_period_total(sample_unclassified_expense_account, sample_unclassified_expense_journal):
    from_date = datetime.now() - timedelta(days=30)
    to_date = datetime.now()
    total = get_last_period_total(sample_unclassified_expense_account, from_date, to_date)
    assert total == 0
