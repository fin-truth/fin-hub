from datetime import datetime, timedelta
from unittest.mock import MagicMock

import pytest

from fin_api import create_app
from fin_api.extensions.database import db
from fin_api.models import Account, BudgetAllotment, JournalEntry


@pytest.fixture(scope="function")
def app():
    app = create_app("TESTING")
    with app.app_context():
        db.create_all()
        yield app
        db.drop_all()


@pytest.fixture(scope="function")
def client(app):
    return app.test_client()


@pytest.fixture(scope="function")
def session(app):
    with app.app_context():
        connection = db.engine.connect()
        transaction = connection.begin()
        yield db.session
        transaction.rollback()
        connection.close()
        db.session.close()


@pytest.fixture
def base_accounts(session):
    accounts = {}
    account = Account(
        name="test_revenue_account",
        human_name="Test Revenue Account",
        description="A test Revenue account",
        balance_type="credit",
        account_type="revenue",
        financial_statement="income_statement",
    )
    session.add(account)
    session.commit()
    accounts["revenue"] = account

    account = Account(
        name="accounts_receivable",
        human_name="Test AR Account",
        description="A test AR account",
        balance_type="debit",
        account_type="noncash_liquid_asset",
        financial_statement="balance_sheet",
    )
    session.add(account)
    session.commit()
    accounts["accounts_receivable"] = account

    account = Account(
        name="common_stock",
        human_name="Test Common Stock Account",
        description="A test Common Stock account",
        balance_type="credit",
        account_type="equity",
        financial_statement="balance_sheet",
    )
    session.add(account)
    session.commit()
    accounts["common_stock"] = account

    account = Account(
        name="interest_expense",
        human_name="Test interest expense Account",
        description="A test interest expense account",
        balance_type="debit",
        account_type="expense",
        financial_statement="income_statement",
    )
    session.add(account)
    session.commit()
    accounts["interest_expense"] = account

    account = Account(
        name="tax_expense",
        human_name="Test tax expense Account",
        description="A test tax expense account",
        balance_type="debit",
        account_type="expense",
        financial_statement="income_statement",
    )
    session.add(account)
    session.commit()
    accounts["tax_expense"] = account

    account = Account(
        name="unrealized_change",
        human_name="Test unrealized change Account",
        description="A test unrealized change account",
        balance_type="credit",
        account_type="noncash_liquid_asset",
        financial_statement="income_statement",
    )
    session.add(account)
    session.commit()
    accounts["unrealized_change"] = account

    account = Account(
        name="change_on_sale",
        human_name="Test change_on_sale Account",
        description="A test change on sale account",
        balance_type="credit",
        account_type="noncash_liquid_asset",
        financial_statement="income_statement",
    )
    session.add(account)
    session.commit()
    accounts["change_on_sale"] = account

    account = Account(
        name="other_comprehensive_income",
        human_name="Test other_comprehensive_income Account",
        description="A test other_comprehensive_income account",
        balance_type="credit",
        account_type="equity",
        financial_statement="balance_sheet",
    )
    session.add(account)
    session.commit()
    accounts["other_comprehensive_income"] = account

    account = Account(
        name="net_income",
        human_name="Test net_income Account",
        description="A test net_income account",
        balance_type="credit",
        account_type="equity",
        financial_statement="balance_sheet",
    )
    session.add(account)
    session.commit()
    accounts["net_income"] = account

    return accounts


@pytest.fixture
def sample_unclassified_expense_account(session):
    account = Account(
        name="test_unclassified_account",
        human_name="Test expense unclassified Account",
        description="A test expense unclassified account",
        balance_type="debit",
        account_type="expense",
        financial_statement="income_statement",
    )
    session.add(account)
    session.commit()
    return account


@pytest.fixture
def sample_misc_expense_account(session):
    account = Account(
        name="test_misc_expense_account",
        human_name="Test misc expense",
        description="A test misc expense account",
        balance_type="debit",
        account_type="misc_expense",
        financial_statement="income_statement",
    )
    session.add(account)
    session.commit()
    return account


@pytest.fixture
def sample_cr_account(session):
    account = Account(
        name="test_cr_account",
        human_name="Test Cr Account",
        description="A test account for credits",
        balance_type="credit",
        account_type="revolving_liability",
        financial_statement="balance_sheet",
    )
    session.add(account)
    session.commit()
    return account


@pytest.fixture
def sample_dr_account(session):
    account = Account(
        name="test_dr_account",
        human_name="Test Dr Account",
        description="A test debit account",
        balance_type="debit",
        account_type="cash_asset",
        financial_statement="balance_sheet",
    )
    session.add(account)
    session.commit()
    return account


@pytest.fixture
def sample_real_estate_accounts(session):
    expense_account = Account(
        name="1130_n_penn_escrow/hoa_expense",
        human_name="Test 1130 Expense Account",
        description="A test account",
        balance_type="debit",
        account_type="expense",
        financial_statement="income_statement",
    )
    note_account = Account(
        name="1130_n_penn_note",
        human_name="Test 1130 Note Account",
        description="A test account",
        balance_type="credit",
        account_type="longterm_liability",
        financial_statement="balance_sheet",
    )
    session.add(expense_account)
    session.add(note_account)
    session.commit()
    return expense_account, note_account


@pytest.fixture
def sample_journal_entry(session, sample_cr_account, sample_dr_account):
    journal_entry = JournalEntry(
        timestamp=datetime.now(),
        description="Test journal entry",
        debited_account_id=sample_dr_account.id,
        debited_amount=100.0,
        credited_account_id=sample_cr_account.id,
        credited_amount=100.0,
    )
    session.add(journal_entry)
    session.commit()
    return journal_entry


@pytest.fixture
def sample_unclassified_expense_journal(session, sample_cr_account, sample_unclassified_expense_account):
    journal_entry = JournalEntry(
        timestamp=datetime.now(),
        description="Test journal entry",
        debited_account_id=sample_unclassified_expense_account.id,
        debited_amount=100.0,
        credited_account_id=sample_cr_account.id,
        credited_amount=100.0,
    )
    session.add(journal_entry)
    session.commit()
    return journal_entry


@pytest.fixture
def sample_budget_allotment(session, sample_account):
    budget_allotment = BudgetAllotment(timestamp=datetime.now(), expense_account_id=sample_account.id, allotment=500.0)
    session.add(budget_allotment)
    session.commit()
    return budget_allotment


@pytest.fixture
def sample_budget_data(sample_unclassified_expense_account):
    return {"account_id": sample_unclassified_expense_account.id, "new_allotment": 1000.0}


@pytest.fixture
def sample_budget_allotments(session, sample_unclassified_expense_account, sample_misc_expense_account):
    allotments = [
        BudgetAllotment(
            timestamp=datetime.now() - timedelta(days=60),
            expense_account_id=sample_unclassified_expense_account.id,
            allotment=500.0,
        ),
        BudgetAllotment(
            timestamp=datetime.now() - timedelta(days=30),
            expense_account_id=sample_unclassified_expense_account.id,
            allotment=700.0,
        ),
        BudgetAllotment(
            timestamp=datetime.now(), expense_account_id=sample_unclassified_expense_account.id, allotment=1000.0
        ),
        BudgetAllotment(timestamp=datetime.now(), expense_account_id=sample_misc_expense_account.id, allotment=5000.0),
    ]
    session.add_all(allotments)
    session.commit()
    return allotments


@pytest.fixture
def mock_balance_sheet():
    mock = MagicMock()
    mock.asset_total = 1000
    mock.liability_total = 500
    return mock


@pytest.fixture
def mock_income_statement():
    mock = MagicMock()
    mock.gross_revenue = 1000
    mock.operating_profit = 500
    mock.net_income = 400
    mock.operating_expenses = {"Expense1": 100, "Expense2": 200}
    mock.gains = {"Gain1": 50, "Gain2": 50}
    return mock
