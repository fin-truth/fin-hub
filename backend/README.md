# Backend
This is the backend of the Fin Hub application. It is a Flask app that uses SQLAlchemy for the ORM, SQLite for the database, and Flask-RESTX for the API.

[![pipeline status](https://gitlab.com/fin-truth/fin-hub/badges/main/pipeline.svg)](https://gitlab.com/fin-truth/fin-hub/-/commits/main)
[![API coverage report](https://gitlab.com/fin-truth/fin-hub/badges/main/coverage.svg)](https://gitlab.com/fin-truth/fin-hub/-/commits/main)
[![Latest Release](https://img.shields.io/badge/dynamic/json?color=blue&label=release&query=%24%5B0%5D.name&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F13102992%2Freleases)](https://gitlab.com/fin-truth/fin-hub/-/releases)

## Getting Started
It is strongly recommended to just use the devcontainer. The `uv` environment will already be set up within this container. If you are developing locally, 
1. Install [uv](https://docs.astral.sh/uv/guides/install-python/) for Python dependency management
2. `uv sync --frozen` to install dependencies.
3. `uv run flask run --host 0.0.0.0` to run the app.

The backend relies on a Redis server to function (caching). The Redis server is defined in the `docker-compose.yml` and in the devcontainer configuration.

## API Endpoints
If you run the API server locally, you can view the Swagger documentation at http://localhost:8008, or whatever port you choose if you are running it locally.
