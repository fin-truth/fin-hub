import fcntl
import os
from datetime import datetime

import click
from flask import Flask, g
from sqlservice import SQLClient
from werkzeug.local import LocalProxy

from fin_api.config import ProdConfig
from fin_api.models import Account, BudgetAllotment
from fin_api.models.base import Model


class FlaskSQLClient(object):
    def __init__(self, app=None, **kwargs):
        self.app = app
        self._db = None
        self.kwargs = kwargs
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        for key, value in SQLClient.DEFAULT_CONFIG.items():
            app.config.setdefault(key, value)

        app.teardown_appcontext(self.teardown_db)

        kwargs_config = self.kwargs.pop("config", {})
        config = {key: app.config[key] for key in SQLClient.DEFAULT_CONFIG.keys()}
        config.update(kwargs_config)

        self._db = SQLClient(config, **self.kwargs)

        # Init database if it doesn't exist. Lock for multi workers not crashing into each other.
        lock_file = "/tmp/db_seed.lock"
        with open(lock_file, "w") as lock:
            fcntl.flock(lock, fcntl.LOCK_EX)
            if not os.path.exists(os.path.join(ProdConfig.INSTANCE_PATH, "app.db")):
                os.makedirs(ProdConfig.INSTANCE_PATH, exist_ok=True)
                with open(os.path.join(ProdConfig.INSTANCE_PATH, "app.db"), "w"):
                    pass
                self._db.create_all()
                if ProdConfig.SEED:
                    seed_database(self._db)
            fcntl.flock(lock, fcntl.LOCK_UN)

    def get_db(self):
        db = getattr(g, "_database", None)
        if db is None:
            db = g._database = self._db
        return db

    @staticmethod
    def teardown_db(_):
        db = getattr(g, "_database", None)
        if db is not None:
            db.close()


client = FlaskSQLClient(model_class=Model)
db = LocalProxy(client.get_db)

default_accounts = [
    Account(
        name="cash",
        human_name="Cash Account",
        balance_type="debit",
        description="Cash assets",
        account_type="cash_asset",
        financial_statement="balance_sheet",
    ),
    Account(
        name="accounts_receivable",
        human_name="Accounts Receivable",
        description="Upcoming cash flows.",
        balance_type="debit",
        account_type="cash_asset",
        financial_statement="balance_sheet",
    ),
    Account(
        name="accounts_payable",
        human_name="Accounts Payable",
        description="Upcoming liabilities.",
        balance_type="credit",
        account_type="liability",
        financial_statement="balance_sheet",
    ),
    Account(
        name="notes_payable",
        human_name="Notes Payable",
        description="Principle amount owed on notes.",
        balance_type="credit",
        account_type="liability",
        financial_statement="balance_sheet",
    ),
    Account(
        name="interest_payable",
        human_name="Interest Payable",
        description="Interest owed on notes.",
        balance_type="credit",
        account_type="liability",
        financial_statement="balance_sheet",
    ),
    Account(
        name="tax_liability",
        human_name="Tax Liability",
        description="Owed tax liability.",
        balance_type="credit",
        account_type="liability",
        financial_statement="income_statement",
    ),
    Account(
        name="accumulated_depreciation",
        human_name="Accumulated Depreciation",
        description="Liability holding accumulated depreciation.",
        balance_type="credit",
        account_type="contra_asset",
        financial_statement="balance_sheet",
    ),
    Account(
        name="retained_earnings",
        human_name="Retained Earnings",
        description="Equity account from profits.",
        balance_type="credit",
        account_type="equity",
        financial_statement="balance_sheet",
    ),
    Account(
        name="common_stock",
        human_name="Common Stock",
        description="Contributed equity.",
        balance_type="credit",
        account_type="equity",
        financial_statement="balance_sheet",
    ),
    Account(
        name="unrealized_gains",
        human_name="Unrealized Gains",
        description="Market value gains in investments.",
        balance_type="credit",
        account_type="equity",
        financial_statement="income_statement",
    ),
    Account(
        name="unrealized_losses",
        human_name="Unrealized Losses",
        description="Market value losses in investments.",
        balance_type="debit",
        account_type="liability",
        financial_statement="income_statement",
    ),
    Account(
        name="gain_on_sale",
        human_name="Gain on Sale",
        description="Gained value on the sale of an asset",
        balance_type="credit",
        account_type="equity",
        financial_statement="income_statement",
    ),
    Account(
        name="loss_on_sale",
        human_name="Loss on Sale",
        description="Lost value on the sale of an asset",
        balance_type="debit",
        account_type="liability",
        financial_statement="income_statement",
    ),
]


def init_db(app: Flask):
    client.init_app(app)


def seed_database(db: SQLClient):
    accounts = default_accounts

    accounts_with_tracked_ids = [
        Account(
            name="unclassified",
            human_name="Unclassified",
            description="",
            balance_type="debit",
            account_type="misc_expense",
            financial_statement="income_statement",
        ),
        Account(
            name="revenue",
            human_name="Revenue",
            description="Cash flows.",
            account_type="equity",
            balance_type="credit",
            financial_statement="income_statement",
        ),
    ]

    expense_accounts = [
        Account(
            name="tax_expense",
            human_name="Tax Expense",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="misc_expense",
        ),
        Account(
            name="accumulated_depreciation_expense",
            human_name="Accumulated Depreciation Expense",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="misc_expense",
        ),
        Account(
            name="groceries",
            human_name="Groceries",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="eating_out",
            human_name="Eating Out",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="interest_expense",
            human_name="Interest Expense",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="misc_expense",
        ),
        Account(
            name="transportation",
            human_name="Transportation",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="fun",
            human_name="Fun",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="bills",
            human_name="Bills",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="trips",
            human_name="Trips",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="household_items",
            human_name="Household Items",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="unexpected_expense",
            human_name="Unexpected Expense",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="healthcare",
            human_name="Healthcare Expenses",
            description="",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="clothes",
            human_name="Clothes",
            description="Clothes",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="luxuries",
            human_name="Luxuries",
            description="Luxuries",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="misc_expense",
        ),
        Account(
            name="haircuts",
            human_name="Haircuts",
            description="Haircuts",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
        Account(
            name="fitness",
            human_name="Fitness",
            description="Fitness",
            balance_type="debit",
            financial_statement="income_statement",
            account_type="expense",
        ),
    ]
    all_accounts = accounts_with_tracked_ids + accounts + expense_accounts
    for account, balance in all_accounts:
        db.session.add(account)
    db.session.commit()
    click.echo("Seeded accounts")

    expense_accounts = db.session.query(Account).filter(Account.account_type.in_(["expense", "misc_expense"])).all()
    for account in expense_accounts:
        allotment = BudgetAllotment(timestamp=datetime.now(), expense_account_id=account.id, allotment=0.0)
        db.session.add(allotment)
    db.session.commit()
    click.echo("Seeded allotments")
