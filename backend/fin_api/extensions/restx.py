from flask_restx import Api

api = Api(
    title="Fin API",
    version="1.0",
    description="A financial management API",
)


def init_restx(app):
    api.init_app(app)
