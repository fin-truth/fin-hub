import os

from flask import Config

APP_ROOT = os.path.dirname(os.path.dirname(__file__))


def config_factory(environment: str):
    if environment == "DEVELOPMENT":
        return DevConfig
    elif environment == "PRODUCTION":
        return ProdConfig
    elif environment == "TESTING":
        return TestConfig
    else:
        raise NotImplementedError(f"Environment {environment} is not supported!")


class ProdConfig(Config):
    INSTANCE_PATH = os.path.join(APP_ROOT, "instance")
    SECRET_KEY = os.environ.get("SECRET_KEY") or "a-key"
    SQL_DATABASE_URI = (
        os.environ.get("DATABASE_URL") or f'sqlite:///{os.path.join(APP_ROOT, os.path.join(INSTANCE_PATH, "app.db"))}'
    )
    SQL_AUTOFLUSH = False
    SQL_POOL_PRE_PING = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    NOT_CLASSIFIED_ACCOUNT_ID = 1
    REVENUE_ACCOUNT = 2
    SEED = False
    TITLE_APPENDAGE = ""
    CACHE_TYPE = "redis"
    CACHE_REDIS_URL = os.environ.get("REDIS_URL", "redis://localhost:6379/0")
    CACHE_DEFAULT_TIMEOUT = 300


class DevConfig(ProdConfig):
    TITLE_APPENDAGE = " Dev"


class TestConfig(ProdConfig):
    TITLE_APPENDAGE = " Test"
    INSTANCE_PATH = os.path.join(APP_ROOT, "instance", "test")
    SQL_DATABASE_URI = "sqlite:///:memory:"
    REVENUE_ACCOUNT = 1
