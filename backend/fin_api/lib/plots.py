import calendar
import json
from datetime import datetime, timedelta
from math import ceil
from typing import List

import plotly
import plotly.graph_objs as go

from fin_api.lib.statements.balance_sheet import BalanceSheet
from fin_api.lib.statements.income_statement import IncomeStatement
from fin_api.lib.tools import (
    cached_get_account_balance,
    get_accounts,
    get_prev_quarter,
    get_quarter,
)


def net_worth_plot():
    yoy_first_of_months = get_first_of_months(n=18)
    balances = [BalanceSheet(up_to, up_to) for up_to in yoy_first_of_months]
    approx_net_worth = [balance.asset_total - balance.liability_total for balance in balances]
    data = [go.Scatter(x=yoy_first_of_months, y=approx_net_worth, mode="lines+markers")]

    graph_json = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    return graph_json


def unrealized_gains_over_time():
    quarters = [get_quarter()[0]]
    for _ in range(15):
        prev_quarter = get_prev_quarter(quarters[0])
        quarters.insert(0, prev_quarter)

    balances = [BalanceSheet(up_to=quarter, previous_up_to=quarter) for quarter in quarters]
    oci = [
        balance
        for sheet in balances
        for account, balance, _ in sheet.equities
        if account.name == "other_comprehensive_income"
    ]
    earnings = []
    for balance in balances:
        accounts = {account.name: amount for account, amount, _ in balance.equities}
        earnings.append(accounts.get("retained_earnings", 0) + accounts.get("net_income", 0))

    data = [
        go.Scatter(
            x=quarters,
            y=oci,
            text=[f"Q{ceil(quarter.month / 4)} {quarter.year}: ${inc:,.2f}" for (quarter, inc) in zip(quarters, oci)],
            name="Other Comp Income",
            mode="lines+markers",
        ),
        go.Scatter(
            x=quarters,
            y=earnings,
            text=[
                f"Q{ceil(quarter.month / 4)} {quarter.year}: ${inc:,.2f}" for (quarter, inc) in zip(quarters, earnings)
            ],
            name="Retained Earnings",
            mode="lines+markers",
        ),
    ]

    graph_json = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    return graph_json


def margins_plot():
    quarters = [get_quarter()[0]]
    for _ in range(15):
        prev_quarter = get_prev_quarter(quarters[0])
        quarters.insert(0, prev_quarter)
    # Need to go back to just before the quarter close for income statements
    quarters = [q - timedelta(milliseconds=1) for q in quarters]

    incomes = [IncomeStatement(up_to=quarter) for quarter in quarters]
    COGS_ACCOUNTS = [
        "Bills",
        "Groceries",
        "Household Items",
        "Healthcare Expenses",
        "Transportation",
        "1130 N Penn Escrow/HOA Expense",
    ]
    cogs_expenses = []
    for income in incomes:
        cogs = [income.operating_expenses.get(exp, 0) for exp in COGS_ACCOUNTS]
        cogs_expenses.append(sum(cogs))

    gross_margin = [
        ((income.gross_revenue - cogs) / income.gross_revenue if income.gross_revenue != 0 else 0)
        for (cogs, income) in zip(cogs_expenses, incomes)
    ]
    op_margin = [
        (income.operating_profit / income.gross_revenue if income.gross_revenue != 0 else 0) for income in incomes
    ]
    net_margin = [income.net_income / income.gross_revenue if income.gross_revenue != 0 else 0 for income in incomes]

    data = [
        go.Scatter(
            x=quarters,
            y=gross_margin,
            text=[
                f"Q{ceil(quarter.month / 4)} {quarter.year}: {margin:.2f}"
                for (quarter, margin) in zip(quarters, gross_margin)
            ],
            name="Gross Margin",
            mode="lines+markers",
        ),
        go.Scatter(
            x=quarters,
            y=op_margin,
            text=[
                f"Q{ceil(quarter.month / 4)} {quarter.year}: {margin:.2f}"
                for (quarter, margin) in zip(quarters, op_margin)
            ],
            name="Operating Margin",
            mode="lines+markers",
        ),
        go.Scatter(
            x=quarters,
            y=net_margin,
            text=[
                f"Q{ceil(quarter.month / 4)} {quarter.year}: {margin:.2f}"
                for (quarter, margin) in zip(quarters, net_margin)
            ],
            name="Net Margin",
            mode="lines+markers",
        ),
    ]

    graph_json = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    return graph_json


def asset_distribution_plot():
    labels = [
        "cash_asset",
        "noncash_liquid_asset",
        "noncash_illiquid_asset",
        "longterm_liability",
        "revolving_liability",
    ]
    values = []
    for label in labels:
        accounts = get_accounts([label])
        value = round(sum([cached_get_account_balance(account) for account in accounts]), 2)
        values.append(value)

    colors = [
        "#F7464A",
        "#46BFBD",
        "#FDB45C",
        "#FEDCBA",
        "#ABCDEF",
        "#DDDDDD",
        "#ABCABC",
    ]
    labels = [label.replace("_", " ").capitalize() for label in labels]
    pie_data = [{"value": value, "label": label, "color": color} for value, label, color in zip(values, labels, colors)]
    graph_json = json.dumps(pie_data)
    return graph_json


def operating_expenses_plot():
    yoy_last_of_months = get_last_of_months(n=13)  # Get 13 months because we will throw first away
    income_statements = [IncomeStatement(up_to) for up_to in yoy_last_of_months]

    # This will accumulate quarter to date expenses and revenues
    accumulated_op_expenses = [sum(statement.operating_expenses.values()) for statement in income_statements]
    accumulated_revenues = [statement.gross_revenue for statement in income_statements]

    # Now we have to get the deltas between months, keeping in mind we zero them out every quarter
    op_expenses = []
    revenues = []
    for i in range(len(accumulated_revenues)):
        if i == 0:  # Throw first away for YoY (we just need previous to correct the first REAL month)
            continue
        elif accumulated_op_expenses[i] - accumulated_op_expenses[i - 1] > 0:
            this_month_expenses = accumulated_op_expenses[i] - accumulated_op_expenses[i - 1]
            this_month_revenues = accumulated_revenues[i] - accumulated_revenues[i - 1]
            op_expenses.append(this_month_expenses)
            revenues.append(this_month_revenues)
        else:
            op_expenses.append(accumulated_op_expenses[i])
            revenues.append(accumulated_revenues[i])

    data = [
        go.Bar(x=yoy_last_of_months, y=revenues, name="Revenue"),
        go.Bar(x=yoy_last_of_months, y=op_expenses, name="Op Expenses"),
    ]
    graph_json = json.dumps(data, cls=plotly.utils.PlotlyJSONEncoder)
    return graph_json


def get_first_of_months(n: int) -> List[datetime]:
    today = datetime.today()
    current_month = today.month
    current_year = today.year
    firsts = [datetime(current_year, today.month, 1)]
    for i in range(n):
        current_month = current_month - 1 if current_month > 1 else 12
        current_year = current_year if current_month != 12 else current_year - 1
        firsts.append(datetime(current_year, current_month, 1))
    firsts.reverse()  # Oldest first
    return firsts


def get_last_of_months(n: int) -> List[datetime]:
    today = datetime.today()
    current_month = today.month
    current_year = today.year
    firsts = [
        datetime(
            current_year,
            today.month,
            calendar.monthrange(current_year, current_month)[1],
            hour=23,
            minute=59,
            second=59,
        )
    ]
    for _ in range(n):
        current_month = current_month - 1 if current_month > 1 else 12
        current_year = current_year if current_month != 12 else current_year - 1
        firsts.append(
            datetime(
                current_year,
                current_month,
                calendar.monthrange(current_year, current_month)[1],
                hour=23,
                minute=59,
                second=59,
            )
        )
    firsts.reverse()  # Oldest first
    return firsts


def histograms_plot():
    quarters = [get_quarter()[0]]
    for _ in range(15):
        prev_quarter = get_prev_quarter(quarters[0])
        quarters.insert(0, prev_quarter)
    # Need to go back to just before the quarter close for income statements
    quarters = [q - timedelta(milliseconds=1) for q in quarters]

    incomes = [IncomeStatement(up_to=quarter) for quarter in quarters]
    revenues = [inc.gross_revenue for inc in incomes]
    op_profit = [inc.operating_profit for inc in incomes]
    gains = [sum(inc.gains.values()) for inc in incomes]
    op_expenses = [sum(inc.operating_expenses.values()) for inc in incomes]

    fig = go.Figure()
    fig.add_trace(go.Histogram(x=revenues, name="Revenues", xbins={"size": 5000}))
    fig.add_trace(go.Histogram(x=op_profit, name="Op Profit", xbins={"size": 5000}))
    fig.add_trace(go.Histogram(x=gains, name="Gains", xbins={"size": 5000}))
    fig.add_trace(go.Histogram(x=op_expenses, name="OpEx", xbins={"size": 5000}))
    fig.update_layout(barmode="overlay")
    fig.update_traces(overwrite=True, marker={"opacity": 0.7})

    graph_json = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)
    return graph_json
