import math
from datetime import date, datetime, timedelta
from typing import List, Tuple

from flask import current_app
from sqlalchemy import desc, func, or_

from fin_api.extensions.cache import cache
from fin_api.extensions.database import db
from fin_api.models import Account, JournalEntry


def slug_name(human_name: str) -> str:
    name = human_name.lower()
    name = name.replace(" ", "_")
    name = name.replace(".", "")
    return name


def first_of_month() -> date:
    today = date.today()
    return date(today.year, today.month, 1)


def last_of_month() -> date:
    today = date.today()
    if (int(today.month) + 1) % 12 < int(today.month):
        first_next_month = date(today.year + 1, 1, 1)
    else:
        first_next_month = date(today.year, (today.month + 1) % 12, 1)
    delta = timedelta(hours=1)
    return first_next_month - delta


def get_current_account_balances() -> List[Tuple[Account, float]]:
    balances = []
    accounts = get_accounts()
    for account in accounts:
        balance = get_account_balance(account)
        balances.append((account, balance))
    return balances


def get_account(account_id: int) -> Account:
    return db.session.query(Account).get(account_id)


def get_accounts(account_types: List[str] = None, include_archived: bool = False) -> List[Account]:
    """Return accounts optionally filtered to a list of account types"""
    if account_types is not None:
        base_query = db.session.query(Account).filter(Account.account_type.in_(account_types))
    else:
        base_query = db.session.query(Account)

    if not include_archived:
        base_query = base_query.filter(Account.is_archived.is_(False))

    return (
        base_query.order_by(Account.is_archived)
        .order_by(Account.human_name)
        .order_by(Account.balance_type.desc())
        .order_by(Account.account_type)
        .all()
    )


def perform_close(on_date: date):
    revenues = get_accounts(["revenue"])
    gross_revenue_account = [rev for rev in revenues if rev.id == current_app.config["REVENUE_ACCOUNT"]][0]
    for revenue in revenues:
        if revenue.id != current_app.config["REVENUE_ACCOUNT"]:
            balance = get_account_balance(revenue, on_date)
            if round(balance, 2) > 0.00:
                revenue_journal = JournalEntry(
                    timestamp=on_date,
                    debited_account_id=revenue.id,
                    debited_amount=balance,
                    credited_account_id=current_app.config["REVENUE_ACCOUNT"],
                    credited_amount=balance,
                    description=f"Close: Merging {revenue.human_name} with gross revenue",
                )
                db.session.add(revenue_journal)

    db.session.commit()
    expenses = get_accounts(["expense", "misc_expense"])
    interest_expense = db.session.query(Account).filter(Account.name == "interest_expense").one()
    tax_expense = db.session.query(Account).filter(Account.name == "tax_expense").one()
    continued_op_expenses = [interest_expense, tax_expense]

    for expense in expenses:
        if expense not in continued_op_expenses:
            balance = get_account_balance(expense, on_date)
            if round(balance, 2) > 0.00:
                expense_journal = JournalEntry(
                    timestamp=on_date,
                    debited_account_id=current_app.config["REVENUE_ACCOUNT"],
                    debited_amount=balance,
                    credited_account_id=expense.id,
                    credited_amount=balance,
                    description=f"Close: Applying balance of {expense.human_name} to revenue",
                )
                db.session.add(expense_journal)

    db.session.commit()

    unrealized_change = db.session.query(Account).filter(Account.name == "unrealized_change").one()
    other_income = db.session.query(Account).filter(Account.name == "other_comprehensive_income").one()
    realized_change = db.session.query(Account).filter(Account.name == "change_on_sale").one()
    balance = get_account_balance(unrealized_change, on_date)
    if abs(round(balance, 2)) > 0.00:
        if balance > 0:
            change_journal = JournalEntry(
                timestamp=on_date,
                debited_account_id=unrealized_change.id,
                debited_amount=abs(balance),
                credited_account_id=other_income.id,
                credited_amount=abs(balance),
                description=f"Close: Applying gain of {unrealized_change.human_name} to other comprehensive income",
            )
        else:
            change_journal = JournalEntry(
                timestamp=on_date,
                debited_account_id=other_income.id,
                debited_amount=abs(balance),
                credited_account_id=unrealized_change.id,
                credited_amount=abs(balance),
                description=f"Close: Applying loss of {unrealized_change.human_name} to other comprehensive income",
            )
        db.session.add(change_journal)

    balance = get_account_balance(realized_change, on_date)
    if abs(round(balance, 2)) > 0.00:
        if balance > 0:
            change_journal = JournalEntry(
                timestamp=on_date,
                debited_account_id=realized_change.id,
                debited_amount=abs(balance),
                credited_account_id=other_income.id,
                credited_amount=abs(balance),
                description=f"Close: Applying gain of {realized_change.human_name} to other comprehensive income",
            )
        else:
            change_journal = JournalEntry(
                timestamp=on_date,
                debited_account_id=other_income.id,
                debited_amount=abs(balance),
                credited_account_id=realized_change.id,
                credited_amount=abs(balance),
                description=f"Close: Applying loss of {realized_change.human_name} to other comprehensive income",
            )
        db.session.add(change_journal)
    db.session.commit()

    for expense in continued_op_expenses:
        balance = get_account_balance(expense, on_date)
        if round(balance, 2) > 0.00:
            expense_journal = JournalEntry(
                timestamp=on_date,
                debited_account_id=current_app.config["REVENUE_ACCOUNT"],
                debited_amount=balance,
                credited_account_id=expense.id,
                credited_amount=balance,
                description=f"Close: Applying balance of {expense.human_name} to pre-tax income",
            )
            db.session.add(expense_journal)
    db.session.commit()

    net_income = db.session.query(Account).filter(Account.name == "net_income").one()
    balance = get_account_balance(gross_revenue_account)
    if balance > 0:
        net_income_journal = JournalEntry(
            timestamp=on_date,
            debited_account_id=gross_revenue_account.id,
            debited_amount=abs(balance),
            credited_account_id=net_income.id,
            credited_amount=abs(balance),
            description=f"Close: Applying net income from {gross_revenue_account.human_name} to Net Income",
        )
    else:
        net_income_journal = JournalEntry(
            timestamp=on_date,
            debited_account_id=net_income.id,
            debited_amount=abs(balance),
            credited_account_id=gross_revenue_account.id,
            credited_amount=abs(balance),
            description=f"Close: Applying net loss from {gross_revenue_account.human_name} to Net Income",
        )
    db.session.add(net_income_journal)
    db.session.commit()


@cache.memoize()
def cached_get_account_balance(account: Account, up_to: datetime = datetime.now() + timedelta(days=1)) -> float:
    return get_account_balance(account, up_to)


def get_account_balance(account: Account, up_to: datetime = datetime.now() + timedelta(days=1)) -> float:
    credit_amount = (
        db.session.query(func.sum(JournalEntry.credited_amount))
        .filter(JournalEntry.credited_account_id == account.id)
        .filter(JournalEntry.timestamp <= up_to)
        .group_by()
        .one_or_none()[0]
    )
    credit_amount = credit_amount if credit_amount is not None else 0.0
    debit_amount = (
        db.session.query(func.sum(JournalEntry.debited_amount))
        .filter(JournalEntry.debited_account_id == account.id)
        .filter(JournalEntry.timestamp <= up_to)
        .group_by()
        .one_or_none()[0]
    )
    debit_amount = debit_amount if debit_amount is not None else 0.0
    if account.balance_type == "credit":
        balance = credit_amount - debit_amount
    else:
        balance = debit_amount - credit_amount
    return balance


def get_categories_list():
    expense_accounts = get_accounts(["expense", "misc_expense"])
    return [(str(expense_account.id), expense_account.human_name) for expense_account in expense_accounts]


def get_date_ranged_items(from_date: date, to_date: date, account_id: int = 0) -> List[JournalEntry]:
    query = (
        db.session.query(JournalEntry)
        .filter(JournalEntry.timestamp >= datetime.combine(from_date, datetime.min.time()))
        .filter(JournalEntry.timestamp <= datetime.combine(to_date, datetime.max.time()))
    )
    if account_id > 0:
        query = query.filter(
            or_(
                JournalEntry.debited_account_id == account_id,
                JournalEntry.credited_account_id == account_id,
            )
        )
    return query.order_by(desc(JournalEntry.timestamp)).all()


def net_cash_at_time(up_to: datetime) -> float:
    cash_accounts = db.session.query(Account).filter(Account.account_type == "cash_asset").all()
    amounts = [get_account_balance(account, up_to) for account in cash_accounts]
    return sum(amounts)


def get_quarter() -> Tuple[datetime, datetime]:
    """Returns beginning of current quarter, and beginning of next quarter"""
    today = date.today()
    quarters = {
        1: datetime(today.year, 1, 1, 0, 0, 0),
        2: datetime(today.year, 4, 1, 0, 0, 0),
        3: datetime(today.year, 7, 1, 0, 0, 0),
        4: datetime(today.year, 10, 1, 0, 0, 0),
        5: datetime(today.year + 1, 1, 1, 0, 0, 0),
    }
    month = today.month
    quarter = math.ceil(month / 3)
    return quarters[quarter], quarters[quarter + 1] - timedelta(milliseconds=1)


def get_next_quarter(quarter: datetime) -> datetime:
    current_quarter = math.ceil(quarter.month / 3)
    next_quarter = current_quarter + 1 if current_quarter != 4 else 1
    month_next_quarter = (next_quarter - 1) * 3 + 1
    year = quarter.year if current_quarter != 4 else quarter.year + 1
    return datetime(year, month_next_quarter, 1, 0, 0, 0)


def get_prev_quarter(quarter: datetime) -> datetime:
    current_quarter = math.ceil(quarter.month / 3)
    prev_quarter = current_quarter - 1 if current_quarter != 1 else 4
    month_next_quarter = (prev_quarter - 1) * 3 + 1
    year = quarter.year if current_quarter != 1 else quarter.year - 1
    return datetime(year, month_next_quarter, 1, 0, 0, 0)


def get_difference_over_time(account: Account, start_date: datetime, end_date: datetime) -> float:
    initial_balance = get_account_balance(account, up_to=start_date)
    ending_balance = get_account_balance(account, up_to=end_date)
    return ending_balance - initial_balance
