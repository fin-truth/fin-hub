from datetime import datetime

from fin_api.extensions.database import db
from fin_api.models import Account, BudgetAllotment, JournalEntry


def get_current_allotment(expense_account: Account) -> BudgetAllotment:
    allotment = (
        db.session.query(BudgetAllotment)
        .filter(BudgetAllotment.expense_account_id == expense_account.id)
        .order_by(BudgetAllotment.timestamp.desc())
        .first()
    )
    return allotment


def get_soft_total(expense_account: Account, from_date: datetime, to_date: datetime) -> float:
    journals = (
        db.session.query(JournalEntry)
        .filter(JournalEntry.debited_account_id == expense_account.id)
        .filter(JournalEntry.timestamp >= from_date)
        .filter(JournalEntry.timestamp <= to_date)
        .all()
    )
    total = sum([journal.credited_amount if journal is not None else 0.0 for journal in journals])
    return total


def get_last_period_total(expense_account: Account, from_date: datetime, to_date: datetime) -> float:
    period = to_date - from_date
    last_period_start = from_date - period
    journals = (
        db.session.query(JournalEntry)
        .filter(JournalEntry.debited_account_id == expense_account.id)
        .filter(JournalEntry.timestamp >= last_period_start)
        .filter(JournalEntry.timestamp <= from_date)
        .all()
    )
    total = sum([journal.credited_amount if journal is not None else 0.0 for journal in journals])
    return total
