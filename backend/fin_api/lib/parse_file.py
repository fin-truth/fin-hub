import calendar
import csv
import io
from abc import ABC, abstractmethod
from datetime import datetime
from typing import Dict, Union

from py_pdf_parser.loaders import load

from fin_api.config import ProdConfig
from fin_api.extensions.database import db
from fin_api.lib.tools import get_account
from fin_api.models import JournalEntry
from fin_api.models.account import Account

SUPPORTED_INPUT_PROVIDERS = [
    "USAA",
    "Venmo",
    "Chase",
    "Schwab",
    "MTBank",
    "Zenefits",
    "Marcus",
]


class ParseError(Exception):
    pass


class ImportParser(ABC):
    def __init__(self, account_id: int, revenue_account_id: int):
        """ABC for implementing new upload parsers for insert into the journal.

        Assumes a debit account, and will credit to the unclassified expense account for debits,
            and to the revenue_account_id for credits.

        Args:
            account_id: DB ID of the account to debit/credit to
            revenue_account_id: DB ID of the revenue account for debits to account_id

        """
        self.account_id = account_id
        self.revenue_account_id = revenue_account_id

    @abstractmethod
    def parse_file(self, file_obj: io.BytesIO):
        pass

    def insert_into_db(
        self,
        entry: Dict,
        debited_account_id: int = ProdConfig.NOT_CLASSIFIED_ACCOUNT_ID,
        credited_account_id: int = 0,
    ):
        if entry:
            date = self.sanitize_date(entry["Date"])
            desc = entry["Original Description"]
            amount = self.sanitize_amount(entry["Amount"])

            if amount > 0:
                item = JournalEntry(
                    timestamp=date,
                    debited_amount=abs(amount),
                    description=desc,
                    debited_account_id=self.account_id,
                    credited_amount=abs(amount),
                    credited_account_id=(credited_account_id if credited_account_id != 0 else self.revenue_account_id),
                )
            else:
                item = JournalEntry(
                    timestamp=date,
                    debited_amount=abs(amount),
                    description=desc,
                    debited_account_id=debited_account_id,
                    credited_amount=abs(amount),
                    credited_account_id=self.account_id,
                )

            db.session.add(item)
            db.session.commit()

    @staticmethod
    def sanitize_amount(raw_amount: str) -> float:
        if isinstance(raw_amount, float):
            return raw_amount
        return float(raw_amount.strip("$").replace(",", ""))

    @staticmethod
    def sanitize_date(date: Union[str, datetime]) -> datetime:
        if isinstance(date, datetime):
            return date
        try:
            return datetime.combine(datetime.strptime(date, "%Y-%m-%d"), datetime.now().time())
        except ValueError:
            pass

        try:
            return datetime.combine(datetime.strptime(date, "%m/%d/%Y"), datetime.now().time())
        except ValueError:
            pass

        raise ValueError(f"Cannot parse date {date}")


class SchwabParser(ImportParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse_file(self, file_obj: io.BytesIO):
        pdf = load(file_obj)

        # Grab some account info for later
        account_name = db.session.query(Account).get(self.account_id).human_name
        unrealized_change = db.session.query(Account).filter(Account.name == "unrealized_change").one()

        # Grab statement month and parse into the right form
        statement_period = pdf.elements.vertically_in_line_with(
            pdf.elements.filter_by_text_contains("Statement Period")[0]
        )[1].text()
        statement_month = statement_period.split(" ")[0]
        statement_year = statement_period.split(" ")[-1]
        statement_date = datetime.strptime(f"{statement_month} 1 {statement_year}", "%B %d %Y")
        entry_date = datetime(
            year=statement_date.year,
            month=statement_date.month,
            day=calendar.monthrange(statement_date.year, statement_date.month)[1],
            hour=23,
            minute=59,
            second=59,
        )

        # Grab dividend and interest amount this month and make an entry for it
        divs_interest_text = pdf.elements.filter_by_text_contains("Dividends and Interest").extract_single_element()
        divs_interest_this_statement = pdf.elements.to_the_right_of(divs_interest_text)[-1].text()
        positive = 1.0 if "(" not in divs_interest_this_statement else -1.0
        amount = positive * float(divs_interest_this_statement.strip("()").replace(",", ""))
        divs_entry = {
            "Date": entry_date,
            "Amount": amount,
            "Original Description": f"{account_name} interest and dividends for {statement_month}",
        }
        self.insert_into_db(divs_entry)

        # Grab unrealized change amount this month and make an entry for it
        unrealized_change_text = pdf.elements.filter_by_text_contains(
            "Market Appreciation/(Depreciation)"
        ).extract_single_element()
        unrealized_change_this_statement = pdf.elements.to_the_right_of(unrealized_change_text)[-1].text()
        positive = 1.0 if "(" not in unrealized_change_this_statement else -1.0
        amount = positive * float(unrealized_change_this_statement.strip("()").replace(",", ""))
        unrealized_entry = {
            "Date": entry_date,
            "Amount": amount,
            "Original Description": f"{account_name} unrealized change for {statement_month}",
        }
        self.insert_into_db(
            unrealized_entry,
            debited_account_id=unrealized_change.id,
            credited_account_id=unrealized_change.id,
        )


class MarcusParser(ImportParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse_file(self, file_obj: io.BytesIO):
        pdf = load(file_obj)

        # Grab some account info for later
        account_name = db.session.query(Account).get(self.account_id).human_name

        # Grab statement month and parse into the right form
        header_block = pdf.elements.filter_by_text_contains("Statement Period")[0].text()
        statement_period = header_block.split("\n")[1]
        end_date = statement_period.split(" to ")[-1]
        end_date = self.sanitize_date(end_date)

        interest = pdf.elements.to_the_right_of(pdf.elements.filter_by_text_contains("Interest Paid this Period")[0])[
            0
        ].text()
        interest_entry = {
            "Date": end_date,
            "Amount": interest,
            "Original Description": f'{account_name} interest and dividends for {end_date.strftime("%B")}',
        }
        self.insert_into_db(interest_entry)


class USAAParser(ImportParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # Check if account is debit or credit
        account = get_account(self.account_id)
        self.balance_type = account.balance_type

    def parse_file(self, file_obj: io.BytesIO):
        try:
            file_obj = io.StringIO(file_obj.read().decode("utf-8"))
        except UnicodeDecodeError as e:
            raise ParseError(e)
        csv_object = csv.DictReader(file_obj)
        data = []
        for line in csv_object:
            if self.balance_type == "credit":
                # USAA lists positive amounts on credit statements
                line["Amount"] = float(line["Amount"]) * -1.0
            data.append(line)
        for entry in data:
            self.insert_into_db(entry)


class MTBankParser(ImportParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse_file(self, file_obj: io.BytesIO):
        try:
            file_obj = io.StringIO(file_obj.read().decode("utf-8"))
        except UnicodeDecodeError as e:
            raise ParseError(e)
        csv_object = csv.DictReader(file_obj)
        for line in csv_object:
            date = line["Date"]
            principal = line["Principal"]
            interest = line["Interest"]
            escrow = line["Escrow"]
            fees = line["Fees"]
            break
        # Get account names
        note_account = db.session.query(Account).filter(Account.name == "1130_n_penn_note").one()
        expense_account = db.session.query(Account).filter(Account.name == "1130_n_penn_escrow/hoa_expense").one()
        date = self.sanitize_date(date)
        principal = self.sanitize_amount(principal)
        iti = float(interest) + float(escrow) + float(fees)
        principal = JournalEntry(
            timestamp=datetime.combine(date, datetime.now().time()),
            debited_amount=abs(principal),
            description=f'{note_account.human_name} principal payment for {date.strftime("%B")}',
            debited_account_id=note_account.id,
            credited_amount=abs(principal),
            credited_account_id=self.account_id,
        )
        iti_journal = JournalEntry(
            timestamp=datetime.combine(date, datetime.now().time()),
            debited_amount=abs(iti),
            description=f'{expense_account.human_name} ITI payment for {date.strftime("%B")}',
            debited_account_id=expense_account.id,
            credited_amount=abs(iti),
            credited_account_id=self.account_id,
        )

        db.session.add(principal)
        db.session.add(iti_journal)
        db.session.commit()


class VenmoParser(ImportParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse_file(self, file_obj: io.BytesIO):
        try:
            file_obj = io.StringIO(file_obj.read().decode("utf-8"))
        except UnicodeDecodeError as e:
            raise ParseError(e)
        csv_object = csv.reader(file_obj)
        data = []
        for line in csv_object:
            if line[1] == "" or line[1] == "ID":
                continue  # Filler lines just skip
            pass
            amount_splits = line[8].split(" ")
            positive = 1.0 if amount_splits[0] == "+" else -1.0
            amount = positive * self.sanitize_amount(amount_splits[1])
            data.append(
                {
                    "Date": line[2].split("T")[0],
                    "Amount": amount,
                    "Original Description": f"{line[5]} From {line[7]} to {line[6]}",
                }
            )
        for entry in data:
            self.insert_into_db(entry)


class ChaseParser(ImportParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse_file(self, file_obj: io.BytesIO):
        try:
            file_obj = io.StringIO(file_obj.read().decode("utf-8"))
        except UnicodeDecodeError as e:
            raise ParseError(e)
        csv_object = csv.reader(file_obj)

        next(csv_object)  # Skip header
        data = []
        for line in csv_object:
            amount = float(line[5])
            data.append({"Date": line[1], "Amount": amount, "Original Description": line[2]})
        for entry in data:
            self.insert_into_db(entry)


class ZenefitsParser(ImportParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def parse_file(self, file_obj: io.BytesIO):
        pdf = load(file_obj)

        # Grab some account info for later
        hsa = db.session.query(Account).filter(Account.name == "health_savings_account").one()
        emp_401k = db.session.query(Account).filter(Account.name == "guideline_401k").one()
        tax_expenses = db.session.query(Account).filter(Account.name == "tax_expense").one()
        healthcare_expenses = db.session.query(Account).filter(Account.name == "healthcare").one()

        # Grab net pay and pay day date
        net_pay_and_date = pdf.elements.to_the_right_of(pdf.elements.filter_by_text_contains("Pay Day:")[0])[0].text()
        _, _, pay_day, net_pay = net_pay_and_date.split("\n")
        pay_day = datetime.combine(datetime.strptime(pay_day, "%b %d, %Y"), datetime.now().time())
        net_pay = float(net_pay.strip("$").replace(",", ""))

        # Grab 401k contribution
        contrib_401k = pdf.elements.to_the_right_of(pdf.elements.filter_by_text_contains("401k")[0])[0].text()
        contrib_401k = float(contrib_401k.strip("$").replace(",", "")) if contrib_401k != "-" else 0

        # Grab hsa contribution
        hsa_entries = pdf.elements.filter_by_text_contains("Health Savings Account")
        for entry in hsa_entries:
            contrib_hsa = pdf.elements.to_the_right_of(entry)[0].text()
            contrib_hsa = float(contrib_hsa.strip("$").replace(",", "")) if contrib_hsa != "-" else 0
            if contrib_hsa > 0:
                hsa_journal = JournalEntry(
                    timestamp=pay_day,
                    debited_amount=abs(contrib_hsa),
                    description=f"{hsa.human_name} contribution",
                    debited_account_id=hsa.id,
                    credited_amount=abs(contrib_hsa),
                    credited_account_id=self.revenue_account_id,
                )
                db.session.add(hsa_journal)

        # Grab dental contribution (on the second page)
        contrib_dental = pdf.elements.to_the_right_of(pdf.elements.filter_by_text_contains("Dental")[1])[0].text()
        contrib_dental = float(contrib_dental.strip("$").replace(",", "")) if contrib_dental != "-" else 0

        # Grab vision contribution (on the second page)
        contrib_vision = pdf.elements.to_the_right_of(pdf.elements.filter_by_text_contains("Vision")[1])[0].text()
        contrib_vision = float(contrib_vision.strip("$").replace(",", "")) if contrib_vision != "-" else 0

        # Grab taxes
        contrib_taxes = pdf.elements.vertically_in_line_with(
            pdf.elements.to_the_right_of(pdf.elements.filter_by_text_contains("Taxes")[0])[0]
        )[-2].text()
        contrib_taxes = float(contrib_taxes.strip("$").replace(",", "").split(" ")[0])

        net_pay = JournalEntry(
            timestamp=pay_day,
            debited_amount=abs(net_pay),
            description=f'Net Pay from AMP Robotics for {pay_day.strftime("%Y-%m-%d")}',
            debited_account_id=self.account_id,
            credited_amount=abs(net_pay),
            credited_account_id=self.revenue_account_id,
        )
        db.session.add(net_pay)
        if contrib_401k > 0:
            journal_401k = JournalEntry(
                timestamp=pay_day,
                debited_amount=abs(contrib_401k),
                description=f"{emp_401k.human_name} employer contribution",
                debited_account_id=emp_401k.id,
                credited_amount=abs(contrib_401k),
                credited_account_id=self.revenue_account_id,
            )
            db.session.add(journal_401k)
        if contrib_dental + contrib_vision > 0:
            healthcare_journal = JournalEntry(
                timestamp=pay_day,
                debited_amount=abs(contrib_dental + contrib_vision),
                description="Employer covered healthcare costs",
                debited_account_id=healthcare_expenses.id,
                credited_amount=abs(contrib_dental + contrib_vision),
                credited_account_id=self.revenue_account_id,
            )
            db.session.add(healthcare_journal)
        tax_journal = JournalEntry(
            timestamp=pay_day,
            debited_amount=abs(contrib_taxes),
            description="Federal and state income taxes",
            debited_account_id=tax_expenses.id,
            credited_amount=abs(contrib_taxes),
            credited_account_id=self.revenue_account_id,
        )
        db.session.add(tax_journal)
        db.session.commit()


def get_parsing_handler(provider: str, account_id: int, revenue_account_id: int) -> ImportParser:
    if provider == "USAA":
        return USAAParser(account_id, revenue_account_id)
    elif provider == "Venmo":
        return VenmoParser(account_id, revenue_account_id)
    elif provider == "Chase":
        return ChaseParser(account_id, revenue_account_id)
    elif provider == "Schwab":
        return SchwabParser(account_id, revenue_account_id)
    elif provider == "MTBank":
        return MTBankParser(account_id, revenue_account_id)
    elif provider == "Zenefits":
        return ZenefitsParser(account_id, revenue_account_id)
    elif provider == "Marcus":
        return MarcusParser(account_id, revenue_account_id)
    else:
        raise ValueError(f"Provider {provider} is not supported.")
