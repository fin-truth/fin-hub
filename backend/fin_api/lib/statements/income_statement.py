import math
from datetime import datetime
from typing import Any, Dict

from fin_api.lib.tools import (
    cached_get_account_balance,
    get_account_balance,
    get_accounts,
)

continuing_operation_expenses = ["interest_expense", "tax_expense"]
gain_loss_accounts = ["unrealized_change", "change_on_sale"]


class IncomeStatement:
    def __init__(self, up_to: datetime = datetime.now(), use_cache: bool = False):
        account_balance = cached_get_account_balance if use_cache else get_account_balance
        all_non_operating_accounts = continuing_operation_expenses + gain_loss_accounts

        # Revenue
        revenue_accounts = get_accounts(["revenue"])
        revenues = {
            account.human_name: account_balance(account, up_to=up_to)
            for account in revenue_accounts
            if not math.isclose(account_balance(account, up_to=up_to), 0, abs_tol=1e-4)
        }

        # Operating expenses
        expense_accounts = [
            account
            for account in get_accounts(["expense", "misc_expense"])
            if account.name not in all_non_operating_accounts
        ]
        operating_expenses = {
            account.human_name: account_balance(account, up_to=up_to)
            for account in expense_accounts
            if not math.isclose(account_balance(account, up_to=up_to), 0, abs_tol=1e-4)
        }

        # Gains/losses
        accounts = [account for account in get_accounts(["noncash_liquid_asset"]) if account.name in gain_loss_accounts]
        gains = {
            account.human_name: account_balance(account, up_to=up_to)
            for account in accounts
            if not math.isclose(account_balance(account, up_to=up_to), 0, abs_tol=1e-4)
        }

        # Continuing Operation Expenses
        continuing_ops_accounts = [
            account
            for account in get_accounts(["expense", "misc_expense"])
            if account.name in continuing_operation_expenses
        ]
        continuing_operating_expenses = {
            account.human_name: account_balance(account, up_to=up_to)
            for account in continuing_ops_accounts
            if not math.isclose(account_balance(account, up_to=up_to), 0, abs_tol=1e-4)
        }
        self.up_to = up_to
        self.revenues = revenues
        self.gross_revenue = sum(revenues.values())
        self.operating_expenses = operating_expenses
        self.operating_profit = self.gross_revenue - sum([expense for expense in operating_expenses.values()])

        self.gains = gains
        self.income_pre_tax = self.operating_profit + sum([gain for gain in gains.values()])

        self.continued_operation_expenses = continuing_operating_expenses

        self.net_income = self.income_pre_tax - sum([expense for expense in continuing_operating_expenses.values()])

    def to_dict(self) -> Dict[str, Any]:
        return {
            "up_to": self.up_to.isoformat(),
            "revenues": self.revenues,
            "total_revenue": self.gross_revenue,
            "operating_expenses": self.operating_expenses,
            "total_operating_expenses": sum(self.operating_expenses.values()),
            "operating_profit": self.operating_profit,
            "gains": self.gains,
            "total_gains": sum(self.gains.values()),
            "income_pre_tax": self.income_pre_tax,
            "continued_operation_expenses": self.continued_operation_expenses,
            "total_continued_operation_expenses": sum(self.continued_operation_expenses.values()),
            "net_income": self.net_income,
        }
