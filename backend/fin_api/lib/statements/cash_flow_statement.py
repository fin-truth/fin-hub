from datetime import datetime
from typing import Any, Dict, List

from fin_api.extensions.database import db
from fin_api.lib.statements.income_statement import IncomeStatement
from fin_api.lib.tools import get_accounts, get_difference_over_time, net_cash_at_time
from fin_api.models import Account


class CashFlow:
    def __init__(self, account_name: str, account_human_name: str, increases_cash_flow: bool, change_in_quarter: float):
        self.account_name = account_name
        self.account_human_name = account_human_name
        self.increases_cash_flow = increases_cash_flow
        self.change_in_quarter = change_in_quarter

    def to_dict(self):
        return {
            "account_name": self.account_name,
            "account_human_name": self.account_human_name,
            "increases_cash_flow": self.increases_cash_flow,
            "change_in_quarter": self.change_in_quarter,
        }


class CashFlowStatement:
    def __init__(
        self,
        income_statement: IncomeStatement,
        quarter_begin: datetime,
        quarter_end: datetime,
    ):
        self.quarter_begin = quarter_begin
        self.quarter_end = quarter_end
        self._income_statement = income_statement
        self.operating_flows = self.calculate_operating_flows()
        self.flow_from_ops = self.sum_cash_flows(self.operating_flows)
        self.investment_flows = self.calculate_investment_flows()
        self.flow_from_investments = self.sum_cash_flows(self.investment_flows)
        self.financing_flows = self.calculate_financing_flows()
        self.flow_from_financing = self.sum_cash_flows(self.financing_flows)
        self.initial_cash_balance = net_cash_at_time(self.quarter_begin)
        self.final_cash_balance = net_cash_at_time(self.quarter_end)
        self.true_difference = self.final_cash_balance - self.initial_cash_balance
        self.total_cash_flow = self.flow_from_ops + self.flow_from_investments + self.flow_from_financing

    def calculate_operating_flows(self) -> List[CashFlow]:
        revolving_liabilities = db.session.query(Account).filter(Account.account_type == "revolving_liability").all()
        accumulated_depreciation = get_accounts(["contra_asset"], include_archived=True)
        cash_flows = [
            CashFlow(account.name, account.human_name, True, self.account_change_in_quarter(account.name))
            for account in accumulated_depreciation
        ] + [
            CashFlow(
                "accounts_receivable",
                "Accounts Receivable",
                False,
                self.account_change_in_quarter("accounts_receivable"),
            )
        ]
        cash_flows += [
            CashFlow(account.name, account.human_name, True, self.account_change_in_quarter(account.name))
            for account in revolving_liabilities
        ]
        cash_flows += [CashFlow("net_income", "Net Income", True, self._income_statement.net_income)]
        return cash_flows

    def calculate_investment_flows(self) -> List[CashFlow]:
        illiquid_assets = get_accounts(["noncash_illiquid_asset", "noncash_liquid_asset"], include_archived=True)
        # Filter out accounts that are already assessed in operations, or show up in account balances
        illiquid_assets = [
            account
            for account in illiquid_assets
            if account.name not in ["accounts_receivable", "change_on_sale", "unrealized_change"]
        ]
        cash_flows = [
            CashFlow(account.name, account.human_name, False, self.account_change_in_quarter(account.name))
            for account in illiquid_assets
        ]
        # Add common stock in
        cash_flows.append(
            CashFlow("common_stock", "Common Stock", True, self.account_change_in_quarter("common_stock"))
        )
        return cash_flows

    def calculate_financing_flows(self) -> List[CashFlow]:
        notes_payable = db.session.query(Account).filter(Account.account_type == "longterm_liability").all()
        return [
            CashFlow(account.name, account.human_name, True, self.account_change_in_quarter(account.name))
            for account in notes_payable
        ]

    def account_change_in_quarter(self, name: str) -> float:
        account = db.session.query(Account).filter(Account.name == name).one()
        change_in_account = get_difference_over_time(account, self.quarter_begin, self.quarter_end)
        return change_in_account

    @staticmethod
    def sum_cash_flows(cash_flows: List[CashFlow]) -> float:
        return sum(
            [
                flow.change_in_quarter if flow.increases_cash_flow else -1.0 * flow.change_in_quarter
                for flow in cash_flows
            ]
        )

    def to_dict(self) -> Dict[str, Any]:
        return {
            "quarter_begin": self.quarter_begin.isoformat(),
            "quarter_end": self.quarter_end.isoformat(),
            "operating_flows": [flow.to_dict() for flow in self.operating_flows],
            "flow_from_ops": self.flow_from_ops,
            "investment_flows": [flow.to_dict() for flow in self.investment_flows],
            "flow_from_investments": self.flow_from_investments,
            "financing_flows": [flow.to_dict() for flow in self.financing_flows],
            "flow_from_financing": self.flow_from_financing,
            "initial_cash_balance": self.initial_cash_balance,
            "final_cash_balance": self.final_cash_balance,
            "true_difference": self.true_difference,
            "total_cash_flow": self.total_cash_flow,
        }
