from datetime import datetime
from typing import List, Tuple

from fin_api.extensions.database import db
from fin_api.lib.tools import cached_get_account_balance, get_account_balance
from fin_api.models import Account


class BalanceSheet:
    def __init__(self, up_to: datetime, previous_up_to: datetime, use_cache: bool = False):
        self._up_to = up_to
        self._previous_up_to = previous_up_to
        self._balance_accounts = db.session.query(Account).filter(Account.financial_statement == "balance_sheet").all()
        self.account_balance = cached_get_account_balance if use_cache else get_account_balance
        self.cash_assets = self.get_accounts_and_balances(["cash_asset"])
        self.noncash_illiquid_assets = self.get_accounts_and_balances(["noncash_illiquid_asset"])
        self.noncash_liquid_assets = self.get_accounts_and_balances(["noncash_liquid_asset"])
        self.contra_assets = self.get_accounts_and_balances(["contra_asset"])
        self.asset_total = (
            sum([b for _, b, _ in self.cash_assets])
            + sum([b for _, b, _ in self.noncash_liquid_assets])
            + sum([b for _, b, _ in self.noncash_illiquid_assets])
            - sum([b for _, b, _ in self.contra_assets])
        )
        self.asset_previous_total = (
            sum([b for _, _, b in self.cash_assets])
            + sum([b for _, _, b in self.noncash_liquid_assets])
            + sum([b for _, _, b in self.noncash_illiquid_assets])
            - sum([b for _, _, b in self.contra_assets])
        )

        self.longterm_liabilities = self.get_accounts_and_balances(["longterm_liability"])
        self.revolving_liabilities = self.get_accounts_and_balances(["revolving_liability"])
        self.liability_total = sum([b for _, b, _ in self.longterm_liabilities]) + sum(
            [b for _, b, _ in self.revolving_liabilities]
        )
        self.liability_previous_total = sum([b for _, _, b in self.longterm_liabilities]) + sum(
            [b for _, _, b in self.revolving_liabilities]
        )

        self.equities = self.get_accounts_and_balances(["equity"])
        self.equity_total = sum([b for _, b, _ in self.equities])
        self.equity_previous_total = sum([b for _, _, b in self.equities])

    def get_accounts_and_balances(self, account_types: List[str]) -> List[Tuple[Account, float, float]]:
        accounts = [account for account in self._balance_accounts if account.account_type in account_types]
        balances = [
            (
                account,
                self.account_balance(account, up_to=self._up_to),
                self.account_balance(account, up_to=self._previous_up_to),
            )
            for account in accounts
        ]
        balances = [
            (account, new, old)
            for account, new, old in balances
            if round(abs(new), 2) > 0.00 or round(abs(old), 2) > 0.00
        ]
        return balances

    def to_dict(self):
        def format_account_balances(balances):
            return [
                {
                    "account_name": account.name,
                    "account_human_name": account.human_name,
                    "current_balance": balance,
                    "previous_balance": prev_balance,
                }
                for account, balance, prev_balance in balances
            ]

        return {
            "assets": {
                "cash_assets": format_account_balances(self.cash_assets),
                "noncash_liquid_assets": format_account_balances(self.noncash_liquid_assets),
                "noncash_illiquid_assets": format_account_balances(self.noncash_illiquid_assets),
                "contra_assets": format_account_balances(self.contra_assets),
                "total": self.asset_total,
                "previous_total": self.asset_previous_total,
            },
            "liabilities": {
                "longterm_liabilities": format_account_balances(self.longterm_liabilities),
                "revolving_liabilities": format_account_balances(self.revolving_liabilities),
                "total": self.liability_total,
                "previous_total": self.liability_previous_total,
            },
            "equity": {
                "accounts": format_account_balances(self.equities),
                "total": self.equity_total,
                "previous_total": self.equity_previous_total,
            },
            "total_liabilities_and_equity": self.liability_total + self.equity_total,
            "total_previous_liabilities_and_equity": self.liability_previous_total + self.equity_previous_total,
            "as_of_date": self._up_to.isoformat(),
            "previous_date": self._previous_up_to.isoformat(),
        }
