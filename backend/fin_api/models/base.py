from sqlalchemy import MetaData
from sqlservice import ModelBase, as_declarative

metadata = MetaData()


@as_declarative(metadata=metadata)
class Model(ModelBase):
    pass
