from datetime import datetime

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from fin_api.models.base import Model
from fin_api.models.mixins import DateMixin


class BudgetAllotment(Model, DateMixin):
    __tablename__ = "budget_allotments"
    id = sa.Column(sa.Integer, primary_key=True)
    allotment = sa.Column(sa.Float(), nullable=False)

    expense_account_id = sa.Column(sa.Integer(), sa.ForeignKey("accounts.id"), nullable=False)
    expense_account = relationship("Account", back_populates="budget_allotments")

    def __init__(
        self,
        timestamp: datetime = None,
        expense_account_id: int = None,
        allotment: float = None,
    ):
        self.timestamp = timestamp
        self.expense_account_id = expense_account_id
        self.allotment = allotment

    def __repr__(self):
        return f"<BudgetAllotment {self.expense_account.human_name}: {self.allotment}>"
