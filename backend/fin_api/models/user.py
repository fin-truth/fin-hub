import sqlalchemy as sa

from fin_api.models.base import Model


class User(Model):
    __tablename__ = "users"
    id = sa.Column(sa.Integer, primary_key=True)
    username = sa.Column(sa.String(64), index=True, unique=True)
    email = sa.Column(sa.String(120), index=True, unique=True)
    password_hash = sa.Column(sa.String(128))

    def __repr__(self):
        return f"<User {self.username}>"
