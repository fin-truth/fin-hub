from datetime import datetime

from sqlalchemy import Column, DateTime


class DateMixin:
    timestamp = Column(DateTime, default=datetime.utcnow)
