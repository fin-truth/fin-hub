from typing import List

import sqlalchemy as sa
from sqlalchemy.orm import relationship

from fin_api.models.base import Model


class Account(Model):
    __tablename__ = "accounts"
    id = sa.Column(sa.Integer, primary_key=True)
    name = sa.Column(sa.String(32), nullable=False, unique=True)
    human_name = sa.Column(sa.String(128), nullable=False, unique=True)
    description = sa.Column(sa.String(256), nullable=True)
    balance_type = sa.Column(sa.Enum("debit", "credit", name="balance_type"), nullable=False)
    account_type = sa.Column(
        sa.Enum(
            "cash_asset",
            "noncash_liquid_asset",
            "noncash_illiquid_asset",
            "equity",
            "revolving_liability",
            "longterm_liability",
            "expense",
            "misc_expense",
            "contra_asset",
            "revenue",
            name="account_type",
        ),
        nullable=False,
    )
    financial_statement = sa.Column(
        sa.Enum("balance_sheet", "income_statement", name="financial_statement"),
        nullable=False,
    )
    is_archived = sa.Column(sa.Boolean, default=False, nullable=False)

    budget_allotments = relationship("BudgetAllotment", back_populates="expense_account")

    def __repr__(self):
        return f"<Account {self.human_name} {self.id}>"

    def to_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "human_name": self.human_name,
            "account_type": self.account_type,
            "financial_statement": self.financial_statement,
            "balance_type": self.balance_type,
            "description": self.description,
            "is_archived": self.is_archived,
        }

    @classmethod
    def get_valid_account_types(cls) -> List[str]:
        return [e for e in cls.account_type.property.columns[0].type.enums]
