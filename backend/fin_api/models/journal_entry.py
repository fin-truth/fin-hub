from datetime import datetime

import sqlalchemy as sa

from fin_api.models.base import Model
from fin_api.models.mixins import DateMixin


class JournalEntry(Model, DateMixin):
    __tablename__ = "journal_entries"
    id = sa.Column(sa.Integer, primary_key=True)
    description = sa.Column(sa.String(128), nullable=False)

    debited_account_id = sa.Column(sa.Integer(), sa.ForeignKey("accounts.id"), nullable=False)

    debited_amount = sa.Column(sa.Float(), nullable=False)

    credited_account_id = sa.Column(sa.Integer(), sa.ForeignKey("accounts.id"), nullable=False)

    credited_amount = sa.Column(sa.Float(), nullable=False)

    def __init__(
        self,
        timestamp: datetime = None,
        debited_account_id: int = None,
        debited_amount: float = None,
        description: str = None,
        credited_account_id: int = None,
        credited_amount: float = None,
    ):
        self.timestamp = timestamp
        self.description = description
        self.debited_account_id = debited_account_id
        self.debited_amount = debited_amount
        self.credited_account_id = credited_account_id
        self.credited_amount = credited_amount

    def __repr__(self):
        return f"<JournalEntry {self.debited_account_id}: {self.debited_amount}>"

    def to_dict(self):
        return {
            "id": self.id,
            "timestamp": self.timestamp.strftime("%Y-%m-%d %H:%M:%S"),
            "description": self.description,
            "debited_account_id": self.debited_account_id,
            "debited_amount": self.debited_amount,
            "credited_account_id": self.credited_account_id,
            "credited_amount": self.credited_amount,
        }
