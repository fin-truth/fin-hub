from datetime import datetime

from flask import request
from flask_restx import Namespace, Resource, fields

from fin_api.extensions.database import db
from fin_api.lib.tools import (
    get_account,
    get_account_balance,
    get_accounts,
    get_current_account_balances,
    slug_name,
)
from fin_api.models import Account

ns_accounts = Namespace("accounts", description="Account operations")

account_model = ns_accounts.model(
    "Account",
    {
        "id": fields.Integer(required=True, description="The account ID"),
        "name": fields.String(required=True, description="The account name"),
        "human_name": fields.String(required=True, description="The human-readable account name"),
        "description": fields.String(description="The account description"),
        "balance_type": fields.String(
            required=True, description="The balance type (debit or credit)", enum=["debit", "credit"]
        ),
        "account_type": fields.String(
            required=True, description="The account type", enum=Account.get_valid_account_types()
        ),
        "financial_statement": fields.String(
            required=True, description="The financial statement type", enum=["balance_sheet", "income_statement"]
        ),
        "is_archived": fields.Boolean(required=True, description="Whether the account is archived"),
    },
)

balance_model = ns_accounts.model(
    "Balance",
    {
        "account": fields.Nested(account_model, required=True, description="The account details"),
        "balance": fields.Float(required=True, description="The current balance of the account"),
    },
)
account_create_model = ns_accounts.model(
    "AccountCreate",
    {
        "human_name": fields.String(required=True, description="The human-readable account name"),
        "description": fields.String(description="The account description"),
        "balance_type": fields.String(
            required=True, description="The balance type (debit or credit)", enum=["debit", "credit"]
        ),
        "account_type": fields.String(
            required=True, description="The account type", enum=Account.get_valid_account_types()
        ),
        "financial_statement": fields.String(
            required=True, description="The financial statement type", enum=["balance_sheet", "income_statement"]
        ),
    },
)


@ns_accounts.route("/balances", endpoint="account_balances")
class AccountBalances(Resource):
    @ns_accounts.doc("get_account_balances")
    @ns_accounts.param("up_to_date", "Get balances up to this date (YYYY-MM-DD)", _in="query")
    @ns_accounts.param("account_id", "Filter by account ID", _in="query", type=int)
    @ns_accounts.marshal_list_with(balance_model)
    def get(self):
        """Get all account balances"""
        up_to_date = request.args.get("up_to_date")
        account_id = request.args.get("account_id", type=int)

        if up_to_date:
            try:
                up_to = datetime.strptime(up_to_date, "%Y-%m-%d")
            except ValueError:
                ns_accounts.abort(400, message="Invalid date format. Use YYYY-MM-DD")
        else:
            balances = get_current_account_balances()

        if account_id:
            account = get_account(account_id)
            if not account:
                ns_accounts.abort(404, message="Account not found")
            if up_to_date:
                balance = get_account_balance(account, up_to=up_to)
                return [{"account": account.to_dict(), "balance": balance}]
            else:
                balances = [(acc, bal) for acc, bal in balances if acc.id == account_id]
        elif up_to_date:
            balances = [
                (account, get_account_balance(account, up_to=up_to)) for account, _ in get_current_account_balances()
            ]

        return [{"account": account.to_dict(), "balance": balance} for account, balance in balances]


@ns_accounts.route("", endpoint="account_list")
class AccountList(Resource):
    @ns_accounts.doc("list_accounts")
    @ns_accounts.param(
        "account_types", "Filter accounts by type(s)", _in="query", action="split", example="expense,cash_asset"
    )
    @ns_accounts.param("include_archived", "Include archived accounts", _in="query", type="boolean", default=False)
    @ns_accounts.marshal_list_with(account_model)
    def get(self):
        """List all accounts"""
        account_types = request.args.getlist("account_types")
        include_archived = request.args.get("include_archived", "false").lower() == "true"
        accounts = get_accounts(
            account_types=account_types if account_types else None, include_archived=include_archived
        )
        return accounts

    @ns_accounts.doc("create_account")
    @ns_accounts.expect(account_create_model)
    @ns_accounts.marshal_with(account_model, code=201)
    def post(self):
        """Create a new account"""
        data = request.json
        account = Account(
            name=slug_name(data["human_name"]),
            human_name=data["human_name"],
            description=data.get("description"),
            balance_type=data["balance_type"],
            account_type=data["account_type"],
            financial_statement=data["financial_statement"],
            is_archived=False,
        )
        db.session.add(account)
        db.session.commit()
        return account, 201


@ns_accounts.route("/<int:id>", endpoint="account_item")
@ns_accounts.response(404, "Account not found")
@ns_accounts.param("id", "The account identifier")
class AccountItem(Resource):
    @ns_accounts.doc("get_account")
    @ns_accounts.marshal_with(account_model)
    def get(self, id):
        """Fetch an account given its identifier"""
        account = db.session.query(Account).get(id)
        if account is None:
            ns_accounts.abort(404, message="Account not found")
        return account

    @ns_accounts.doc("update_account")
    @ns_accounts.expect(account_model)
    @ns_accounts.marshal_with(account_model)
    def put(self, id):
        """Update an account given its identifier"""
        account = db.session.query(Account).get(id)
        if account is None:
            ns_accounts.abort(404, message="Account not found")
        data = request.json
        account.human_name = data["human_name"]
        account.description = data.get("description")
        account.balance_type = data["balance_type"]
        account.account_type = data["account_type"]
        account.financial_statement = data["financial_statement"]

        # Handle is_archived conversion
        if "is_archived" in data:
            if isinstance(data["is_archived"], bool):
                account.is_archived = data["is_archived"]
            elif isinstance(data["is_archived"], str):
                if data["is_archived"].lower() == "true":
                    account.is_archived = True
                elif data["is_archived"].lower() == "false":
                    account.is_archived = False
                else:
                    ns_accounts.abort(400, message="is_archived must be 'true' or 'false'")
            else:
                ns_accounts.abort(400, message="is_archived must be a boolean or 'true'/'false' string")

        db.session.commit()
        return account
