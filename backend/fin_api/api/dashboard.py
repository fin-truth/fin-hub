from flask_restx import Namespace, Resource, fields

from fin_api.lib.plots import (
    asset_distribution_plot,
    histograms_plot,
    margins_plot,
    net_worth_plot,
    operating_expenses_plot,
    unrealized_gains_over_time,
)

ns_dashboard = Namespace("dashboard", description="Dashboard operations")

plot_model = ns_dashboard.model(
    "Plot", {"data": fields.String(required=True, description="JSON string containing plot data")}
)

dashboard_model = ns_dashboard.model(
    "Dashboard",
    {
        "net_worth": fields.Nested(plot_model, required=True, description="Net worth plot data"),
        "operating_expenses": fields.Nested(plot_model, required=True, description="Operating expenses plot data"),
        "asset_distribution": fields.Nested(plot_model, required=True, description="Asset distribution plot data"),
        "margins": fields.Nested(plot_model, required=True, description="Margins plot data"),
        "unrealized_gains": fields.Nested(plot_model, required=True, description="Unrealized gains plot data"),
        "histograms": fields.Nested(plot_model, required=True, description="Histograms plot data"),
    },
)


@ns_dashboard.route("")
class Dashboard(Resource):
    @ns_dashboard.doc("get_dashboard_data")
    @ns_dashboard.marshal_with(dashboard_model)
    def get(self):
        """Get dashboard data including various plots"""
        data = {
            "net_worth": {"data": net_worth_plot()},
            "operating_expenses": {"data": operating_expenses_plot()},
            "asset_distribution": {"data": asset_distribution_plot()},
            "margins": {"data": margins_plot()},
            "unrealized_gains": {"data": unrealized_gains_over_time()},
            "histograms": {"data": histograms_plot()},
        }
        return data
