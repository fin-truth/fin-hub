from datetime import datetime

from flask import request
from flask_restx import Namespace, Resource, fields

from fin_api.extensions.database import db
from fin_api.lib.budget_tools import (
    get_current_allotment,
    get_last_period_total,
    get_soft_total,
)
from fin_api.lib.tools import first_of_month, get_accounts, last_of_month
from fin_api.models import Account, BudgetAllotment

ns_budget = Namespace("budget", description="Budget related operations")


budget_model = ns_budget.model(
    "Budget",
    {
        "id": fields.Integer(required=True, description="The account ID"),
        "category": fields.String(
            required=True, description="The name of the expense account that the budget allotment refers to"
        ),
        "account_type": fields.String(
            required=True, description=f'The account type ({",".join(Account.get_valid_account_types())})'
        ),
        "last_period_total": fields.Float(
            required=True, description="Amount spent against the account in the previous period"
        ),
        "allotment": fields.Float(required=True, description="The budget allotment set for this account"),
        "soft_total": fields.Float(required=True, description="The current amount spent against the account"),
        "balance": fields.Float(required=True, description="The allotment - soft_total for the current period"),
    },
)


@ns_budget.route("", endpoint="budget_list")
class BudgetList(Resource):
    @ns_budget.doc("get_budget_data")
    @ns_budget.marshal_list_with(budget_model)
    def get(self):
        """Get budget data"""
        from_date = request.args.get("from_date", first_of_month().strftime("%Y-%m-%d"))
        to_date = request.args.get("to_date", last_of_month().strftime("%Y-%m-%d"))
        include_misc = request.args.get("include_misc", "false").lower() == "true"

        from_date = datetime.strptime(from_date, "%Y-%m-%d").date()
        to_date = datetime.strptime(to_date, "%Y-%m-%d").date()

        account_types = ["expense", "misc_expense"] if include_misc else ["expense"]
        expense_accounts = get_accounts(account_types)
        budget_data = []

        for account in expense_accounts:
            last_period_total = get_last_period_total(account, from_date, to_date)
            current_allotment = get_current_allotment(account)
            soft_total = get_soft_total(account, from_date, to_date)
            balance = current_allotment.allotment - soft_total if current_allotment else 0

            budget_data.append(
                {
                    "id": account.id,
                    "category": account.human_name,
                    "account_type": account.account_type,
                    "last_period_total": last_period_total,
                    "allotment": current_allotment.allotment if current_allotment else 0.0,
                    "soft_total": soft_total,
                    "balance": balance,
                }
            )

        return budget_data


allotment_model = ns_budget.model(
    "Allotment",
    {
        "account_id": fields.Integer(required=True, description="The account ID that the allotment refers to"),
        "new_allotment": fields.Float(required=True, description="The new budget allotment value to set"),
    },
)


@ns_budget.route("/allotment", endpoint="budget_allotment")
class BudgetAllotmentRoute(Resource):
    @ns_budget.doc("update_allotment")
    @ns_budget.expect(allotment_model)
    @ns_budget.marshal_with(allotment_model)
    def put(self):
        """Update budget allotment"""
        data = request.json
        account_id = data.get("account_id")
        new_allotment = data.get("new_allotment")

        if not account_id or new_allotment is None:
            ns_budget.abort(400, "Missing account_id or new_allotment")

        account = db.session.query(Account).filter(Account.id == account_id).first()
        if not account:
            ns_budget.abort(404, "Account not found")

        allotment = BudgetAllotment(timestamp=datetime.now(), allotment=new_allotment, expense_account_id=account_id)
        db.session.add(allotment)
        db.session.commit()

        return {"account_id": account_id, "new_allotment": new_allotment}
