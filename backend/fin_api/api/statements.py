from datetime import datetime, timedelta

from flask import request
from flask_restx import Namespace, Resource, fields

from fin_api.lib.statements.balance_sheet import BalanceSheet
from fin_api.lib.statements.cash_flow_statement import CashFlowStatement
from fin_api.lib.statements.income_statement import IncomeStatement
from fin_api.lib.tools import get_next_quarter, get_quarter, perform_close

ns_statements = Namespace("statements", description="Financial statement operations")

# Balance Sheet model
balance_sheet_model = ns_statements.model(
    "BalanceSheet",
    {
        "assets": fields.Raw(required=True, description="Assets breakdown"),
        "liabilities": fields.Raw(required=True, description="Liabilities breakdown"),
        "equity": fields.Raw(required=True, description="Equity breakdown"),
        "total_liabilities_and_equity": fields.Float(required=True, description="Total liabilities and equity"),
        "total_previous_liabilities_and_equity": fields.Float(
            required=True, description="Total previous liabilities and equity"
        ),
        "as_of_date": fields.DateTime(required=True, description="As of date"),
        "previous_date": fields.DateTime(required=True, description="Previous date"),
    },
)

# Income Statement model
income_statement_model = ns_statements.model(
    "IncomeStatement",
    {
        "up_to": fields.DateTime(required=True, description="As of date"),
        "revenues": fields.Raw(required=True, description="Revenue breakdown"),
        "total_revenue": fields.Float(required=True, description="Total revenue"),
        "operating_expenses": fields.Raw(required=True, description="Operating expenses breakdown"),
        "total_operating_expenses": fields.Float(required=True, description="Total operating expenses"),
        "operating_profit": fields.Float(required=True, description="Operating profit"),
        "gains": fields.Raw(required=True, description="Gains breakdown"),
        "total_gains": fields.Float(required=True, description="Total gains"),
        "income_pre_tax": fields.Float(required=True, description="Income pre-tax"),
        "continued_operation_expenses": fields.Raw(required=True, description="Continued operation expenses breakdown"),
        "total_continued_operation_expenses": fields.Float(
            required=True, description="Total continued operation expenses"
        ),
        "net_income": fields.Float(required=True, description="Net income"),
    },
)

# Cash Flow Statement model
cash_flow_statement_model = ns_statements.model(
    "CashFlowStatement",
    {
        "quarter_begin": fields.DateTime(required=True, description="Quarter start date"),
        "quarter_end": fields.DateTime(required=True, description="Quarter end date"),
        "operating_flows": fields.Raw(required=True, description="Operating cash flows"),
        "flow_from_ops": fields.Float(required=True, description="Net cash flow from operations"),
        "investment_flows": fields.Raw(required=True, description="Investment cash flows"),
        "flow_from_investments": fields.Float(required=True, description="Net cash flow from investments"),
        "financing_flows": fields.Raw(required=True, description="Financing cash flows"),
        "flow_from_financing": fields.Float(required=True, description="Net cash flow from financing"),
        "initial_cash_balance": fields.Float(required=True, description="Initial cash balance"),
        "final_cash_balance": fields.Float(required=True, description="Final cash balance"),
        "true_difference": fields.Float(
            required=True, description="True difference between initial and final cash balance"
        ),
        "total_cash_flow": fields.Float(required=True, description="Total net cash flow"),
    },
)


@ns_statements.route("/balance-sheet", endpoint="statements_balance_sheet")
class BalanceSheetResource(Resource):
    @ns_statements.param("quarter", "Quarter number (1-4)", _in="query")
    @ns_statements.param("year", "Year (e.g., 2023)", _in="query")
    @ns_statements.marshal_with(balance_sheet_model)
    def get(self):
        quarter = request.args.get("quarter")
        year = request.args.get("year")

        if quarter and year:
            quarter_begin = datetime(int(year), (int(quarter) - 1) * 3 + 1, 1, 0, 0, 0)
            next_quarter_start = get_next_quarter(quarter_begin)
        else:
            quarter_begin, quarter_end = get_quarter()
            next_quarter_start = quarter_end + timedelta(milliseconds=2)

        balance_sheet = BalanceSheet(up_to=next_quarter_start, previous_up_to=quarter_begin)
        return balance_sheet.to_dict()


@ns_statements.route("/income-statement", endpoint="statements_income_statement")
class IncomeStatementResource(Resource):
    @ns_statements.param("quarter", "Quarter number (1-4)", _in="query")
    @ns_statements.param("year", "Year (e.g., 2023)", _in="query")
    @ns_statements.marshal_with(income_statement_model)
    def get(self):
        quarter = request.args.get("quarter")
        year = request.args.get("year")

        if quarter and year:
            quarter_begin = datetime(int(year), (int(quarter) - 1) * 3 + 1, 1, 0, 0, 0)
            quarter_end = get_next_quarter(quarter_begin) - timedelta(milliseconds=1)
        else:
            _, quarter_end = get_quarter()

        statement = IncomeStatement(quarter_end)
        return statement.to_dict()


@ns_statements.route("/cash-flow", endpoint="statements_cash_flow")
class CashFlowStatementResource(Resource):
    @ns_statements.param("quarter", "Quarter number (1-4)", _in="query")
    @ns_statements.param("year", "Year (e.g., 2023)", _in="query")
    @ns_statements.marshal_with(cash_flow_statement_model)
    def get(self):
        quarter = request.args.get("quarter")
        year = request.args.get("year")

        if quarter and year:
            quarter_begin = datetime(int(year), (int(quarter) - 1) * 3 + 1, 1, 0, 0, 0)
            quarter_end = get_next_quarter(quarter_begin) - timedelta(milliseconds=1)
        else:
            quarter_begin, quarter_end = get_quarter()

        income_statement = IncomeStatement(quarter_end)
        cash_flow = CashFlowStatement(income_statement, quarter_begin, quarter_end)
        return cash_flow.to_dict()


@ns_statements.route("/close", endpoint="statements_close")
class CloseStatementResource(Resource):
    @ns_statements.expect(
        ns_statements.model("CloseDate", {"date": fields.Date(required=True, description="Close date")})
    )
    def post(self):
        date = request.json.get("date")
        if not date:
            return {"error": "Date is required"}, 400

        try:
            close_date = datetime.strptime(date, "%Y-%m-%d")
            perform_close(close_date)
            return {"message": "Close performed successfully"}, 200
        except Exception as e:
            return {"error": str(e)}, 500
