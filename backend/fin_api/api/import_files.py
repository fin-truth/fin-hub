from flask import request
from flask_restx import Namespace, Resource, fields
from werkzeug.datastructures import FileStorage

from fin_api.lib.parse_file import (
    SUPPORTED_INPUT_PROVIDERS,
    ParseError,
    get_parsing_handler,
)

ns_import_files = Namespace("import", description="File import operations")
# Input model
import_file_input = ns_import_files.model(
    "ImportFileInput",
    {
        "provider": fields.String(required=True, description="Provider name", enum=SUPPORTED_INPUT_PROVIDERS),
        "account_id": fields.Integer(required=True, description="Account ID"),
        "revenue_account_id": fields.Integer(required=True, description="Revenue Account ID"),
    },
)

# Success response model
import_file_success = ns_import_files.model(
    "ImportFileSuccess", {"message": fields.String(description="Success message")}
)

# Error response model
import_file_error = ns_import_files.model("ImportFileError", {"error": fields.String(description="Error message")})

# File upload parser
file_upload = ns_import_files.parser()
file_upload.add_argument("file", location="files", type=FileStorage, required=True)


@ns_import_files.route("", endpoint="import_file")
class ImportFile(Resource):
    @ns_import_files.expect(file_upload, import_file_input)
    @ns_import_files.response(200, "Success", import_file_success)
    @ns_import_files.response(400, "Validation Error", import_file_error)
    def post(self):
        """Import a file"""
        if "file" not in request.files:
            return {"error": "No file part"}, 400

        file = request.files["file"]
        if file.filename == "":
            return {"error": "No selected file"}, 400

        provider = request.form.get("provider")
        account_id = request.form.get("account_id")
        revenue_account_id = request.form.get("revenue_account_id")

        if not all([provider, account_id, revenue_account_id]):
            return {"error": "Missing required fields"}, 400

        try:
            parsing_handler = get_parsing_handler(
                provider,
                int(account_id),
                int(revenue_account_id),
            )
            parsing_handler.parse_file(file.stream)

            return {"message": "File imported successfully"}, 200
        except ParseError as e:
            return {"error": str(e)}, 400


@ns_import_files.route("/providers", endpoint="supported_providers")
class SupportedProviders(Resource):
    @ns_import_files.doc("get_supported_providers")
    def get(self):
        """Get list of supported providers"""
        return SUPPORTED_INPUT_PROVIDERS
