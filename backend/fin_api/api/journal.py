from datetime import datetime

from flask import abort, request
from flask_restx import Namespace, Resource, fields

from fin_api.extensions.database import db
from fin_api.models import JournalEntry

ns_journal = Namespace("journal", description="Journal operations")

journal_entry_model = ns_journal.model(
    "JournalEntry",
    {
        "id": fields.Integer(readonly=True, description="The journal entry identifier"),
        "timestamp": fields.DateTime(required=True, description="Timestamp of the journal entry"),
        "description": fields.String(required=True, description="Description of the journal entry"),
        "debited_account_id": fields.Integer(required=True, description="ID of the debited account"),
        "debited_amount": fields.Float(required=True, description="Amount debited"),
        "credited_account_id": fields.Integer(required=True, description="ID of the credited account"),
        "credited_amount": fields.Float(required=True, description="Amount credited"),
    },
)

journal_entry_create_model = ns_journal.model(
    "JournalEntryCreate",
    {
        "timestamp": fields.String(required=True, description="Timestamp of the journal entry (YYYY-MM-DD HH:MM)"),
        "description": fields.String(required=True, description="Description of the journal entry"),
        "debited_account_id": fields.Integer(required=True, description="ID of the debited account"),
        "debited_amount": fields.Float(required=True, description="Amount debited"),
        "credited_account_id": fields.Integer(required=True, description="ID of the credited account"),
        "credited_amount": fields.Float(required=True, description="Amount credited"),
    },
)


@ns_journal.route("", endpoint="journal_entry_list")
class JournalEntryList(Resource):
    @ns_journal.doc("list_journal_entries")
    @ns_journal.param("from_date", "Start date (YYYY-MM-DD)", _in="query")
    @ns_journal.param("to_date", "End date (YYYY-MM-DD)", _in="query")
    @ns_journal.param("account_id", "Filter by account ID", _in="query", type=int)
    @ns_journal.marshal_list_with(journal_entry_model)
    def get(self):
        """List journal entries"""
        from_date = request.args.get("from_date")
        to_date = request.args.get("to_date")
        account_id = request.args.get("account_id")

        query = db.session.query(JournalEntry)

        if from_date and to_date:
            try:
                from_date = datetime.strptime(from_date, "%Y-%m-%d")
                to_date = datetime.strptime(to_date, "%Y-%m-%d")
                query = query.filter(JournalEntry.timestamp.between(from_date, to_date))
            except ValueError:
                abort(400, description="Invalid date format. Use YYYY-MM-DD")
        elif from_date or to_date:
            abort(400, description="Both from_date and to_date must be provided")

        if account_id:
            query = query.filter(
                (JournalEntry.debited_account_id == account_id) | (JournalEntry.credited_account_id == account_id)
            )

        entries = query.order_by(JournalEntry.timestamp.desc()).all()
        return entries

    @ns_journal.doc("create_journal_entry")
    @ns_journal.expect(journal_entry_create_model)
    @ns_journal.marshal_with(journal_entry_model, code=201)
    def post(self):
        """Create a new journal entry"""
        data = request.json
        try:
            new_entry = JournalEntry(
                timestamp=datetime.strptime(data["timestamp"], "%Y-%m-%d %H:%M"),
                description=data["description"],
                debited_account_id=int(data["debited_account_id"]),
                debited_amount=float(data["debited_amount"]),
                credited_account_id=int(data["credited_account_id"]),
                credited_amount=float(data["credited_amount"]),
            )
            db.session.add(new_entry)
            db.session.commit()
            return new_entry, 201
        except KeyError as e:
            abort(400, description=f"Missing required field: {str(e)}")
        except ValueError as e:
            abort(400, description=f"Invalid data format: {str(e)}")
        except Exception as e:
            db.session.rollback()
            abort(500, description=f"An error occurred while creating the journal entry: {str(e)}")


@ns_journal.route("/<int:id>", endpoint="journal_entry_item")
@ns_journal.response(404, "Journal entry not found")
@ns_journal.param("id", "The journal entry identifier")
class JournalEntryItem(Resource):
    @ns_journal.doc("get_journal_entry")
    @ns_journal.marshal_with(journal_entry_model)
    def get(self, id):
        """Fetch a journal entry given its identifier"""
        entry = db.session.query(JournalEntry).get(id)
        if entry is None:
            abort(404, description="Journal entry not found")
        return entry

    @ns_journal.doc("update_journal_entry")
    @ns_journal.expect(journal_entry_model)
    @ns_journal.marshal_with(journal_entry_model)
    def put(self, id):
        """Update a journal entry given its identifier"""
        entry = db.session.query(JournalEntry).get(id)
        if entry is None:
            abort(404, description="Journal entry not found")
        data = request.json
        try:
            entry.timestamp = datetime.strptime(data["timestamp"], "%Y-%m-%d %H:%M")
            entry.description = data["description"]
            entry.debited_account_id = int(data["debited_account_id"])
            entry.debited_amount = float(data["debited_amount"])
            entry.credited_account_id = int(data["credited_account_id"])
            entry.credited_amount = float(data["credited_amount"])

            db.session.commit()
            return entry
        except KeyError as e:
            abort(400, description=f"Missing required field: {str(e)}")
        except ValueError as e:
            abort(400, description=f"Invalid data format: {str(e)}")
        except Exception as e:
            db.session.rollback()
            abort(500, description=f"An error occurred while updating the journal entry: {str(e)}")

    @ns_journal.doc("delete_journal_entry")
    @ns_journal.response(204, "Journal entry deleted")
    def delete(self, id):
        """Delete a journal entry given its identifier"""
        entry = db.session.query(JournalEntry).get(id)
        if entry is None:
            abort(404, description="Journal entry not found")
        try:
            db.session.delete(entry)
            db.session.commit()
            return "", 204
        except Exception as e:
            db.session.rollback()
            abort(500, description=f"An error occurred while deleting the journal entry: {str(e)}")
