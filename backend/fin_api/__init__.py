import logging
import os

from flask import Flask
from flask_cors import CORS

from fin_api.api import (
    ns_accounts,
    ns_budget,
    ns_dashboard,
    ns_import_files,
    ns_journal,
    ns_statements,
)
from fin_api.config import config_factory
from fin_api.extensions.cache import init_cache
from fin_api.extensions.database import init_db
from fin_api.extensions.restx import api, init_restx


def create_app(environment: str = os.getenv("ENVIRONMENT", "DEVELOPMENT")):
    config = config_factory(environment)
    app = Flask(
        __name__,
        instance_path=config.INSTANCE_PATH,
        static_folder=os.path.join("frontend", "static"),
        template_folder=os.path.join("frontend", "templates"),
    )
    CORS(
        app,
        resources={r"/api/*": {"origins": "*"}},
        allow_headers=["Content-Type", "Authorization"],
        supports_credentials=True,
    )

    # Apply config
    app.config.from_object(config_factory(environment))
    log_level = os.getenv("LOG_LEVEL", "INFO").upper()
    logging.basicConfig(level=getattr(logging, log_level))
    logging.getLogger("pdfminer").setLevel(logging.INFO)

    # Init extensions
    init_db(app)
    init_cache(app)
    init_restx(app)

    # Add namespaces to Flask-RESTX API
    api.add_namespace(ns_accounts, path="/api/1/accounts")
    api.add_namespace(ns_budget, path="/api/1/budget")
    api.add_namespace(ns_dashboard, path="/api/1/dashboard")
    api.add_namespace(ns_journal, path="/api/1/journal")
    api.add_namespace(ns_import_files, path="/api/1/import")
    api.add_namespace(ns_statements, path="/api/1/statements")

    return app
