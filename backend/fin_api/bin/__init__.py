import sys

import click

from fin_api.bin.export_data import export_cli, export_statements_cli
from fin_api.bin.import_models import import_models_cli


@click.group("fin-utils")
def fin_utils():
    pass


fin_utils.add_command(export_cli)
fin_utils.add_command(export_statements_cli)
fin_utils.add_command(import_models_cli)


if __name__ == "__main__":
    fin_utils(sys.argv[1:])
