import csv
import os
from typing import Type

import click

from fin_api import create_app
from fin_api.extensions.database import db
from fin_api.lib.statements.income_statement import calculate_income_statement
from fin_api.lib.tools import get_prev_quarter, get_quarter
from fin_api.models import Account, BudgetAllotment, JournalEntry, Model


@click.command("export-models")
@click.argument("export_path")
def export_cli(export_path):
    os.makedirs(export_path, exist_ok=True)
    app = create_app()
    with app.app_context():

        def export_table(model: Type[Model], export_path: str):
            columns = model.__table__.columns.keys()
            export_file = os.path.join(export_path, f"{model.__tablename__}.csv")
            rows = db.session.query(model).all()
            model_data = [[getattr(item, column) for column in columns] for item in rows]
            with open(export_file, "w") as f:
                writer = csv.writer(f)
                writer.writerow(columns)
                for row in model_data:
                    writer.writerow(row)

        tables = [Account, BudgetAllotment, JournalEntry]
        for model in tables:
            export_table(model, export_path)


@click.command("export-statements")
@click.argument("export_path")
def export_statements_cli(export_path):
    os.makedirs(export_path, exist_ok=True)
    app = create_app()
    with app.app_context():
        this_quarter, _ = get_quarter()
        last_quarter = get_prev_quarter(this_quarter)
        two_quarters_ago = get_prev_quarter(last_quarter)
        three_quarters_ago = get_prev_quarter(two_quarters_ago)
        last_four_quarters = [
            three_quarters_ago,
            two_quarters_ago,
            last_quarter,
            this_quarter,
        ]
        inc_statements = [calculate_income_statement(q) for q in last_four_quarters]
        with open(os.path.join(export_path, "income_statements.csv"), "w") as f:
            writer = csv.writer(f, delimiter=",")
            for statement in inc_statements:
                for row in statement.serialize_to_csv():
                    writer.writerow(row)
