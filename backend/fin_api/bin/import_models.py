import csv
import logging
import os
from datetime import datetime
from typing import List, Type

import click

from fin_api import create_app
from fin_api.extensions.database import db
from fin_api.models import Account, BudgetAllotment, JournalEntry, Model

logger = logging.getLogger(__name__)


@click.command("import")
@click.argument("import_path", type=click.Path(exists=True))
def import_models_cli(import_path):
    files = os.listdir(import_path)
    app = create_app()
    with app.app_context():

        def import_table(model: Type[Model], model_data: List[List[str]]):
            columns = model.__table__.columns.keys()
            header = model_data.pop(0)
            if header != columns:
                raise ValueError(f"Header does not match columns for {model.__tablename__}. {header} vs. {columns}")
            for row in model_data:
                model_object = model()
                for column_name, item in zip(header, row):
                    if column_name == "timestamp":
                        try:
                            item = datetime.strptime(item, "%Y-%m-%d %H:%M:%S.%f")
                        except ValueError:
                            item = datetime.strptime(item, "%Y-%m-%d %H:%M:%S")
                    if column_name == "is_archived":
                        item = True if item == "True" else False
                    setattr(model_object, column_name, item)
                db.session.add(model_object)
                db.session.commit()

        data = {file_name: load_data(os.path.join(import_path, file_name)) for file_name in files}
        click.echo("Importing accounts...")
        import_table(Account, data["accounts.csv"])
        click.echo("Importing allotments...")
        import_table(BudgetAllotment, data["budget_allotments.csv"])
        click.echo("Importing journals...")
        import_table(JournalEntry, data["journal_entries.csv"])


def load_data(path: str) -> List[List[str]]:
    with open(path, "r") as f:
        reader = csv.reader(f)
        data = [row for row in reader]
    return data
